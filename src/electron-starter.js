const electron = require('electron');
// Module to control application life.
const app = electron.app;
// Module to create native browser window.
const BrowserWindow = electron.BrowserWindow;
const { ipcMain } = electron;

const path = require('path');
const url = require('url');

var fs = require('fs-extra');
const PDFDocument = require('pdfkit');

var pdfMake = require('pdfmake/build/pdfmake');
var pdfFonts = require('pdfmake/build/vfs_fonts');

pdfMake.vfs = pdfFonts.pdfMake.vfs;

const Excel = require('exceljs');
let mainWindow;
let reference = "";
let vifrex_tag = "";
let supplier_code = "";
let doc = new PDFDocument;
let firstPage = true;
var purchase_order;

// this should be placed at top of main.js to handle setup events quickly
if (handleSquirrelEvent(app)) {
    // squirrel event handled and app will exit in 1000ms, so don't do anything else
    return;
}

electron.Menu.setApplicationMenu(null);

function createWindow() {
    // Create the browser window.
    mainWindow = new BrowserWindow({
        width: 1500, height: 700, minWidth: 1500, minHeight: 700, icon: __dirname + '/../www/assets/logo_dark.png', webPreferences: {
            contextIsolation: false,
            nodeIntegration: true,
            backgroundThrottling: false
        }
    });

    // and load the index.html of the app.
    const startUrl = process.env.ELECTRON_START_URL || url.format({
        pathname: path.join(__dirname, '/../www/index.html'),
        protocol: 'file:',
        slashes: true
    });

    mainWindow.loadURL(startUrl);
    // Open the DevTools.
    //mainWindow.webContents.openDevTools();

    // Emitted when the window is closed.
    mainWindow.on('closed', function () {
        // Dereference the window object, usually you would store windows
        // in an array if your app supports multi windows, this is the time
        // when you should delete the corresponding element.
        mainWindow = null
    })
}


ipcMain.on('Cards', (events, arg) => {
    if (firstPage) {
        doc.image(arg, {
            fit: [320, 742]
        });
        firstPage = false;
    } else {
        doc.addPage();
        doc.image(arg, {
            fit: [320, 742]
        });
    }
    events.returnValue = "OK pdf";
})

ipcMain.on('PrintPDF', (events, arg) => {
    doc.pipe(fs.createWriteStream(app.getPath('desktop') + '/VXOrders/' + reference + '/Supplier_' + supplier_code + '/SPO_Tags_' + supplier_code + '.pdf'))
        .on('finish', function () {
            console.log('PDF closed');
        });

    doc.end();
    events.returnValue = "Printed pdf";
})

ipcMain.on('PrintSelectedCards', (events, arg) => {
    doc.pipe(fs.createWriteStream(app.getPath('desktop') + '/' + arg))
        .on('finish', function () {
            console.log('PDF closed Selected Cards');
        });

    doc.end();
    events.returnValue = "Printed pdf";
})

ipcMain.on('InitDoc', (events, arg) => {
    doc = new PDFDocument({
        layout: 'landscape',
        size: [141, 327],
        margin: 0
    });
    firstPage = true;
    events.returnValue = "OK Init";
})

function base64_encode(file) {
    // read binary data
    var bitmap = fs.readFileSync(file);
    // convert binary data to base64 encoded string
    return new Buffer(bitmap).toString('base64');
}

function buildPaymentTermBody(data) {
    var body = [];
    var headerArray = [];

    if (data.length > 0) {
        headerArray.push({ text: 'Payment terms:', style: 'tableHeader', decoration: 'underline' }, `${data[0].payment_term_entry}`);
        body.push(headerArray);

        data.shift();

        data.forEach((element) => {
            var dataArray = [];
            dataArray.push({}, `${element.payment_term_entry}`);
            body.push(dataArray);
        });
    } else {
        headerArray.push({ text: 'Payment terms:', style: 'tableHeader', decoration: 'underline' }, ' ');
        body.push(headerArray);
    }

    return body;
};

function buildFabricsBodies(data) {

    var bodies = [];

    data.forEach((element) => {
        var body = [];

        var fabric_info = element;

        let fabric_code = '-';
        let fabric_ex_mill = '-';
        let fabric_eta = '-';
        let fabric_color = '-';
        let fabric_length = '-';
        let fabric_inco_term = '-';
        let fabric_price = '-';
        let fabric_total = '-';

        if (typeof fabric_info.vifrex_code !== 'undefined') {
            fabric_code = fabric_info.vifrex_code;
        }
        if (typeof fabric_info.date_ex_mill !== 'undefined') {
            fabric_ex_mill = fabric_info.date_ex_mill;
        }
        if (typeof fabric_info.eta !== 'undefined') {
            fabric_eta = fabric_info.eta;
        }
        if (typeof fabric_info.color !== 'undefined') {
            fabric_color = fabric_info.color;
        }
        if (typeof fabric_info.length !== 'undefined') {
            fabric_length = fabric_info.length;
        }
        if (typeof fabric_info.inco_term !== 'undefined') {
            fabric_inco_term = fabric_info.inco_term;
        }
        if (typeof fabric_info.price !== 'undefined') {
            fabric_price = fabric_info.price;
        }
        if (typeof fabric_info.total !== 'undefined') {
            fabric_total = fabric_info.total;
        }

        var dataArray0 = [];
        dataArray0.push({ text: 'Vifrex Code', style: 'tableHeader', alignment: 'center' }, { text: 'Color', style: 'tableHeader', alignment: 'center' }, { text: 'Quantity (mts)', style: 'tableHeader', alignment: 'center' }, { text: 'Ex mill', style: 'tableHeader', alignment: 'center' }, { text: 'ETA', style: 'tableHeader', alignment: 'center' }, { text: 'Incoterm', style: 'tableHeader', alignment: 'center' }, { text: 'Price (€)', style: 'tableHeader', alignment: 'center' }, { text: 'Total (€)', style: 'tableHeader', alignment: 'center' });
        body.push(dataArray0);

        var dataArray1 = [];
        dataArray1.push({ text: `${fabric_code}`, alignment: 'center' }, { text: `${fabric_color}`, alignment: 'center' }, { text: `${fabric_length}`, alignment: 'center' }, { text: `${fabric_ex_mill}`, alignment: 'center' }, { text: `${fabric_eta}`, alignment: 'center' }, { text: `${fabric_inco_term}`, alignment: 'center' }, { text: `${fabric_price}`, alignment: 'center' }, { text: `${fabric_total}`, alignment: 'center' });
        body.push(dataArray1);

        bodies.push(body);
    });
    return bodies;
};

ipcMain.on('PrintPurchaseOrder', (events, arg) => {

    var purchase_order = arg;
    var image = purchase_order[0];
    let image_string = image[0].image_string;

    var sup_fabric = purchase_order[1];

    var fabricsRowData = purchase_order[2];
    var paymentTermsRowData = purchase_order[3];
    var summaryData = purchase_order[4];

    let po_reference = sup_fabric[0].po_reference;
    let po_date = sup_fabric[0].po_date;
    let supplier_name = sup_fabric[0].supplier_name;
    let supplier_code = sup_fabric[0].supplier_code;
    let country = sup_fabric[0].country;
    let address = sup_fabric[0].address;
    let phone_number = sup_fabric[0].phone_number;
    let postal_code = sup_fabric[0].postal_code;
    let vifrex_code = sup_fabric[0].vifrex_code;
    let fabric_supplier_code = sup_fabric[0].fabric_supplier_code;
    let composition = sup_fabric[0].composition;
    let construction = sup_fabric[0].construction;
    let width = sup_fabric[0].width;
    let weight = sup_fabric[0].weight;
    let type = sup_fabric[0].type;

    var fabrics_tables = buildFabricsBodies(fabricsRowData);

    let comments;
    let total;
    if (typeof summaryData[0].total !== 'undefined') {
        total = summaryData[0].total;
    }
    if (typeof summaryData[0].comments !== 'undefined') {
        comments = summaryData[0].comments;
    }

    var docDefinition = {
        background: function (page) {
            if (page === 1) {
                return [
                    {
                        canvas: [
                            {
                                type: 'rect',
                                x: 35,
                                y: 140,
                                w: 525,
                                h: 665,
                                r: 4,
                                lineColor: 'black',
                            },
                        ]
                    }
                ];
            } else {
                return [
                    {
                        canvas: [
                            {
                                type: 'rect',
                                x: 35,
                                y: 35,
                                w: 525,
                                h: 770,
                                r: 4,
                                lineColor: 'black',
                            },
                        ]
                    }
                ];
            }
        },
        footer: function (currentPage, pageCount) {
            return {
                table: {
                    body: [
                        [
                            { text: "Sales Conditions:	*Goods can be delivered ±5% per colour way.	*Our quality control system is according to 4 point system.	*All Bank fees & commissions on the customer side shall be paid by customer.	*The maximum storage of ready goods is ten days, Vifrex holds the right to invoice after ten days of storage.	*The Block orders will be kept at customers liability till confirmed date. In the end of this period the unused part will be directly invocied to the customer.	* If the production is for babywear, this information need to be advised at the beginning of order. Otherwise no liability will be accepted.	*If required, any additional certification must be stated prior to the order. Vifrex accepts no liability regarding certifications after the order is in the process. *Delivery of the goods will not be put in the process unless the buyer sends back a sealed and signed copy of order confirmation.", alignment: 'left', style: 'footerHeader', }
                        ],
                    ]
                },
                layout: 'noBorders'
            };
        },
        content: [],
        styles: {
            header: {
                fontSize: 9,
                bold: true,
                margin: [0, 0, 0, 10]
            },
            subheader: {
                fontSize: 9,
                bold: true,
                margin: [0, 10, 0, 5]
            },
            tableExample: {
                fontSize: 9,
                margin: [0, 5, 0, 15]
            },
            tableFabrics: {
                fontSize: 8,
                margin: [0, 5, 0, 15]
            },
            tableHeader: {
                bold: true,
                fontSize: 9,
                color: 'black'
            },
            priceHeader: {
                bold: true,
                fontSize: 9,
                color: 'black'
            },
            footerHeader: {
                bold: true,
                fontSize: 5,
                margin: [40, 8, 40, 15],
                color: 'black',
                alignment: 'justify'
            },
        },
    }

    docDefinition.content.push({
        image: 'data:image/png;base64,' + base64_encode(__dirname + image_string),
        width: 520,
    });

    docDefinition.content.push(' ');

    docDefinition.content.push({
        style: 'tableExample',
        table: {
            widths: [120, 375],
            headerRows: 0,
            body: [
                [{ text: 'PO Reference:', style: 'tableHeader' }, `${po_reference}`],
                [{ text: 'PO Date:', style: 'tableHeader' }, `${po_date}`],
            ]
        },
        layout: 'noBorders'
    });

    docDefinition.content.push({
        style: 'tableExample',
        table: {
            widths: [120, 375],
            headerRows: 0,
            body: [
                [{ text: 'Supplier:', style: 'tableHeader' }, `${supplier_name}`],
                [{ text: 'Supplier Code:', style: 'tableHeader' }, `${supplier_code}`],
                [{ text: 'Country:', style: 'tableHeader' }, `${country}`],
                [{ text: 'Address:', style: 'tableHeader' }, `${address}`],
                [{ text: 'Postal Code:', style: 'tableHeader' }, `${postal_code}`],
                [{ text: 'Phone:', style: 'tableHeader' }, `${phone_number}`],
            ]
        },
        layout: {
            hLineWidth: function (i, node) {
                return (i === 0 || i === node.table.body.length) ? 1.2 : 0;
            },
            vLineWidth: function (i, node) {
                return (i === 0 || i === node.table.widths.length) ? 1.2 : 0;
            },
            hLineColor: function (i, node) {
                return (i === 0 || i === node.table.body.length) ? 'black' : 'white';
            },
            vLineColor: function (i, node) {
                return (i === 0 || i === node.table.widths.length) ? 'black' : 'white';
            },
        }
    });

    docDefinition.content.push({
        style: 'tableExample',
        table: {
            widths: [120, 375],
            headerRows: 0,
            body: [
                [{ text: 'Vifrex Code:', style: 'tableHeader' }, `${vifrex_code}`],
                [{ text: 'Fabric code:', style: 'tableHeader' }, `${fabric_supplier_code}`],
                [{ text: 'Composition:', style: 'tableHeader' }, `${composition}`],
                [{ text: 'Construction:', style: 'tableHeader' }, `${construction}`],
                [{ text: 'Width:', style: 'tableHeader' }, `${width}`],
                [{ text: 'Weight:', style: 'tableHeader' }, `${weight}`],
                [{ text: 'Type:', style: 'tableHeader' }, `${type}`],
            ]
        },
        layout: {
            hLineWidth: function (i, node) {
                return (i === 0 || i === node.table.body.length) ? 1.2 : 0;
            },
            vLineWidth: function (i, node) {
                return (i === 0 || i === node.table.widths.length) ? 1.2 : 0;
            },
            hLineColor: function (i, node) {
                return (i === 0 || i === node.table.body.length) ? 'black' : 'white';
            },
            vLineColor: function (i, node) {
                return (i === 0 || i === node.table.widths.length) ? 'black' : 'white';
            },
        }
    });

    fabrics_tables.forEach((element) => {
        docDefinition.content.push({
            style: 'tableFabrics',
            table: {
                widths: [53, 53, 60, 56, 55, 56, 54, 55],
                headerRows: 1,
                body: element
            },
            layout: {
                hLineWidth: function (i, node) {
                    return (i === 0 || i === node.table.body.length) ? 2 : 0.5;
                },
                vLineWidth: function (i, node) {
                    return (i === 0 || i === node.table.widths.length) ? 2 : 0.5;
                },
                hLineColor: function (i, node) {
                    return (i === 0 || i === node.table.body.length) ? 'white' : 'white';
                },
                vLineColor: function (i, node) {
                    return (i === 0 || i === node.table.widths.length) ? 'white' : 'grey';
                },
                fillColor: function (rowIndex, node, columnIndex) {
                    return (rowIndex % 2 === 1) ? '#CCCCCC' : null;
                }
            }
        });
    })

    docDefinition.content.push({
        style: 'tableExample',
        table: {
            widths: [375, 120],
            headerRows: 1,
            body: [
                [{ text: 'Observations:', style: 'tableHeader', alignment: 'left' }, { text: 'Total (€):', style: 'tableHeader', alignment: 'left' }],
                [{ text: `${comments}`, alignment: 'justify' }, { text: `${total}`, alignment: 'center' }],
            ]
        },
        layout: {
            hLineWidth: function (i, node) {
                return (i === 0 || i === node.table.body.length) ? 2 : 0.5;
            },
            vLineWidth: function (i, node) {
                return (i === 0 || i === node.table.widths.length) ? 2 : 0.5;
            },
            hLineColor: function (i, node) {
                return (i === 0 || i === node.table.body.length) ? 'white' : 'white';
            },
            vLineColor: function (i, node) {
                return (i === 0 || i === node.table.widths.length) ? 'white' : 'grey';
            },
            fillColor: function (rowIndex, node, columnIndex) {
                return (rowIndex % 2 === 1) ? '#CCCCCC' : null;
            }
        }
    });

    docDefinition.content.push({
        style: 'tableExample',
        table: {
            headerRows: 0,
            body: buildPaymentTermBody(paymentTermsRowData)
        },
        layout: 'noBorders'
    });

    var pdf = pdfMake.createPdf(docDefinition);

    var PODir = app.getPath('desktop') + '/PurchaseOrders/';
    

    if (!fs.existsSync(PODir)) {
        fs.mkdirSync(PODir);
    }

    let year = po_reference.substring(2, 6);
    let month = po_reference.substring(6, 8);
    var PODirMonth = PODir + year + '_' + month + '/';

    if (!fs.existsSync(PODirMonth)) {
        fs.mkdirSync(PODirMonth);
    }

    var dir = PODirMonth + po_reference + '.pdf';

    if (fs.existsSync(dir)) {
        fs.remove(dir, function (error) { });
    }

    setTimeout(function () {
        pdf.getBase64((data) => {
            let writeStream = fs.createWriteStream(dir);

            // write some data with a base64 encoding
            writeStream.write(data, 'base64');

            // the finish event is emitted when all data has been flushed from the stream
            writeStream.on('finish', () => {
                console.log('Wrote PO PDF');
            });

            // close the stream
            writeStream.end();
            events.returnValue = "PDF PO Printed Succesfully";
        });
    }, 1000)
})

ipcMain.on('Supplier', (events, arg) => {
    supplier_code = arg;
    var orderDir = app.getPath('desktop') + '/VXOrders/' + reference + '/Supplier_' + supplier_code;

    if (!fs.existsSync(orderDir)) {
        fs.mkdirSync(orderDir);
    }
    doc = new PDFDocument({
        layout: 'landscape',
        size: [141, 327],
        margin: 0
    });
    firstPage = true;
    events.returnValue = "OK Supplier";
})

ipcMain.on('Tag', (events, arg) => {
    const today = new Date();
    const dd = String(today.getDate()).padStart(2, '0');
    const mm = String(today.getMonth() + 1).padStart(2, '0');
    const yyyy = today.getFullYear();
    doc.pipe(fs.createWriteStream(app.getPath('desktop') + '/VXTags/' + yyyy + '_' + mm + '_' + dd + '/' + vifrex_tag + '.pdf'))
        .on('finish', function () {
            console.log('PDF closed Tag');
        });

    doc.end();
    events.returnValue = "OK Tag printed";
})

ipcMain.on('Tag_code', (events, arg) => {

    vifrex_tag = arg;
    var orderDir = app.getPath('desktop') + '/VXTags/';

    if (!fs.existsSync(orderDir)) {
        fs.mkdirSync(orderDir);
    }

    const today = new Date();
    const dd = String(today.getDate()).padStart(2, '0');
    const mm = String(today.getMonth() + 1).padStart(2, '0');
    const yyyy = today.getFullYear();
    orderDir = app.getPath('desktop') + '/VXTags/' + yyyy + '_' + mm + '_' + dd + '/';

    if (!fs.existsSync(orderDir)) {
        fs.mkdirSync(orderDir);
    }
    events.returnValue = "OK directory created";
})

ipcMain.on('vifrexSPO', (event, arg) => {

    var input_order = arg[0];

    reference = input_order[0].reference;
    date = input_order[0].date_order;

    var orderDir = app.getPath('desktop') + '/VXOrders/';
    var dir = orderDir + reference;

    if (!fs.existsSync(orderDir)) {
        fs.mkdirSync(orderDir);
    }

    if (fs.existsSync(dir)) {
        fs.remove(dir, function (error) { });
    }

    setTimeout(function () {
        if (!fs.existsSync(dir)) {
            fs.mkdirSync(dir);
        }
    }, 1000)

    setTimeout(function () {

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////EXCEL SPO VISITA
        //Excel Title, Header, Data
        let title = 'Order: ' + reference;
        let header = ["Vifrex ID", "Sample Yardage", "Hanger", "Comments"]
        let data = [];
        input_order.forEach((element) => {
            let dataRow = [element.vifrex_id, element.sampleyardage, element.hanger, element.comments_order]
            data.push(dataRow)
        })

        //Create workbook and worksheet
        workbook = new Excel.Workbook();
        let worksheet = workbook.addWorksheet('SPO Data');


        //Add Row and formatting
        let titleRow = worksheet.addRow([title]);
        titleRow.font = {
            name: 'Arial Black',
            family: 4,
            size: 18,
            bold: true
        }
        titleRow.getCell(1).fill = {
            type: 'pattern',
            pattern: 'solid',
            fgColor: { argb: 'FFFFFF00' },
            bgColor: { argb: 'FF0000FF' }
        };



        var logo = workbook.addImage({
            filename: __dirname + '/assets/VX_International.png',
            extension: 'png',
        });

        worksheet.addImage(logo, 'E1:F6');
        worksheet.mergeCells('A1:B2');
        worksheet.mergeCells('E1:F6');


        let headerRowDate = worksheet.addRow(['DATE', 'COMPANY', 'DEPARTMENT', 'NAME']);
        headerRowDate.eachCell((cell, number) => {
            cell.fill = {
                type: 'pattern',
                pattern: 'solid',
                fgColor: { argb: 'FFFFFF00' },
                bgColor: { argb: 'FF0000FF' }
            }
            cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
        })
        worksheet.addRow(['Date : ' + date, input_order[0].company, input_order[0].department, input_order[0].name])

        worksheet.addRow([]);
        worksheet.addRow([]);

        //Add Header Row
        let headerRow = worksheet.addRow(header);

        // Cell Style : Fill and Border
        headerRow.eachCell((cell, number) => {
            cell.fill = {
                type: 'pattern',
                pattern: 'solid',
                fgColor: { argb: 'FFFFFF00' },
                bgColor: { argb: 'FF0000FF' }
            }
            cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
        })
        // worksheet.addRows(data);


        // Add Data and Conditional Formatting
        data.forEach(d => {
            let row = worksheet.addRow(d);
        });

        worksheet.getColumn(1).width = 30;
        worksheet.getColumn(2).width = 30;
        worksheet.getColumn(3).width = 30;
        worksheet.getColumn(4).width = 30;
        worksheet.addRow([]);


        //Footer Row
        let footerRow = worksheet.addRow(['This is an order made on: ' + date]);
        footerRow.getCell(1).fill = {
            type: 'pattern',
            pattern: 'solid',
            fgColor: { argb: 'FFCCFFE5' }
        };
        footerRow.getCell(1).border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }

        //Merge Cells
        worksheet.mergeCells(`A${footerRow.number}:F${footerRow.number}`);

        workbook
            .xlsx
            .writeFile(dir + '/SPOVisit.xlsx')
            .then(() => {
            })
            .catch((err) => {
                console.log(err)
            });
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////EXCELS SPO INTERN
        //Excel Title, Header, Data
        title = 'Order: ' + reference;
        header = ["Vifrex ID", "Supplier fabric Code", "Supplier", "Sample Yardage", "Hanger", "Comments"]
        data = [];
        input_order.forEach((element) => {
            let dataRow = [element.vifrex_id, element.fabric_supplier_code, element.supplier_name, element.sampleyardage, element.hanger, element.comments_order]
            data.push(dataRow)
        })

        //Create workbook and worksheet
        workbook = new Excel.Workbook();
        worksheet = workbook.addWorksheet('SPO Data');


        //Add Row and formatting
        titleRow = worksheet.addRow([title]);
        titleRow.font = {
            name: 'Arial Black',
            family: 4,
            size: 18,
            bold: true
        }
        titleRow.getCell(1).fill = {
            type: 'pattern',
            pattern: 'solid',
            fgColor: { argb: 'FFFFFF00' },
            bgColor: { argb: 'FF0000FF' }
        };



        logo = workbook.addImage({
            filename: __dirname + '/assets/VX_International.png',
            extension: 'png',
        });

        worksheet.addImage(logo, 'F1:G6');
        worksheet.mergeCells('A1:B2');
        worksheet.mergeCells('F1:G6');


        headerRowDate = worksheet.addRow(['DATE', 'COMPANY', 'DEPARTMENT', 'NAME']);
        headerRowDate.eachCell((cell, number) => {
            cell.fill = {
                type: 'pattern',
                pattern: 'solid',
                fgColor: { argb: 'FFFFFF00' },
                bgColor: { argb: 'FF0000FF' }
            }
            cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
        })
        worksheet.addRow(['Date : ' + date, input_order[0].company, input_order[0].department, input_order[0].name])

        worksheet.addRow([]);
        worksheet.addRow([]);

        //Add Header Row
        headerRow = worksheet.addRow(header);

        // Cell Style : Fill and Border
        headerRow.eachCell((cell, number) => {
            cell.fill = {
                type: 'pattern',
                pattern: 'solid',
                fgColor: { argb: 'FFFFFF00' },
                bgColor: { argb: 'FF0000FF' }
            }
            cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
        })
        // worksheet.addRows(data);


        // Add Data and Conditional Formatting
        data.forEach(d => {
            let row = worksheet.addRow(d);
        });

        worksheet.getColumn(1).width = 30;
        worksheet.getColumn(2).width = 30;
        worksheet.getColumn(3).width = 35;
        worksheet.getColumn(4).width = 30;
        worksheet.getColumn(5).width = 30;
        worksheet.addRow([]);


        //Footer Row
        footerRow = worksheet.addRow(['This is an order made on: ' + date]);
        footerRow.getCell(1).fill = {
            type: 'pattern',
            pattern: 'solid',
            fgColor: { argb: 'FFCCFFE5' }
        };
        footerRow.getCell(1).border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }

        //Merge Cells
        worksheet.mergeCells(`A${footerRow.number}:F${footerRow.number}`);

        //Generate Excel File with given name
        workbook
            .xlsx
            .writeFile(dir + '/SPOVifrex.xlsx')
            .then(() => {
            })
            .catch((err) => {
                console.log(err)
            });
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////EXCEL SPOS SUPPLIERS

        arg.splice(0, 1)
        excelSuppliers = arg;
        excelSuppliers.forEach(list => {
            var DirSupplier = app.getPath('desktop') + '/VXOrders/' + reference + '/Supplier_' + list[0].supplier;

            if (!fs.existsSync(DirSupplier)) {
                fs.mkdirSync(DirSupplier);
            }

            //Excel Title, Header, Data
            let title = 'Order: ' + reference + ' / ' + list[0].supplier + ' / ' + list[0].supplier_name;
            let header = ["Fabric code", "Sample Yardage", "Hanger", "Comments"]
            let data = [];
            list.forEach((element) => {
                let dataRow = [element.fabric_supplier_code, element.sampleyardage, element.hanger, element.comments_order]
                data.push(dataRow)
            })

            //Create workbook and worksheet
            workbook = new Excel.Workbook();
            let worksheet = workbook.addWorksheet('SPO Data');


            //Add Row and formatting
            let titleRow = worksheet.addRow([title]);
            titleRow.font = {
                name: 'Arial Black',
                family: 4,
                size: 18,
                bold: true
            }
            titleRow.getCell(1).fill = {
                type: 'pattern',
                pattern: 'solid',
                fgColor: { argb: 'FFFFFF00' },
                bgColor: { argb: 'FF0000FF' }
            };

            var logo = workbook.addImage({
                filename: __dirname + '/assets/VX_International.png',
                extension: 'png',
            });

            worksheet.addImage(logo, 'E1:F6');
            worksheet.mergeCells('A1:C2');
            worksheet.mergeCells('E1:F6');


            let headerRowDate = worksheet.addRow(['DATE', 'COMPANY']);
            headerRowDate.eachCell((cell, number) => {
                cell.fill = {
                    type: 'pattern',
                    pattern: 'solid',
                    fgColor: { argb: 'FFFFFF00' },
                    bgColor: { argb: 'FF0000FF' }
                }
                cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
            })
            worksheet.addRow(['Date : ' + date, 'Vifrex Studio'])

            worksheet.addRow([]);
            worksheet.addRow([]);

            //Add Header Row
            let headerRow = worksheet.addRow(header);

            // Cell Style : Fill and Border
            headerRow.eachCell((cell, number) => {
                cell.fill = {
                    type: 'pattern',
                    pattern: 'solid',
                    fgColor: { argb: 'FFFFFF00' },
                    bgColor: { argb: 'FF0000FF' }
                }
                cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
            })
            // worksheet.addRows(data);


            // Add Data and Conditional Formatting
            data.forEach(d => {
                let row = worksheet.addRow(d);
            });

            worksheet.getColumn(1).width = 30;
            worksheet.getColumn(2).width = 30;
            worksheet.getColumn(3).width = 30;
            worksheet.getColumn(4).width = 30;
            worksheet.addRow([]);


            //Footer Row
            let footerRow = worksheet.addRow(['This is an order made on: ' + date]);
            footerRow.getCell(1).fill = {
                type: 'pattern',
                pattern: 'solid',
                fgColor: { argb: 'FFCCFFE5' }
            };
            footerRow.getCell(1).border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }

            //Merge Cells
            worksheet.mergeCells(`A${footerRow.number}:F${footerRow.number}`);

            //Generate Excel File with given name
            workbook
                .xlsx
                .writeFile(DirSupplier + '/SPOSupplier_' + list[0].supplier + '.xlsx')
                .then(() => {
                })
                .catch((err) => {
                    console.log(err)
                });

        })
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    }, 2500)
})

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', function () {
    // On OS X it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
        app.quit()
    }
});

app.on('activate', function () {
    // On OS X it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (mainWindow === null) {
        createWindow()
    }
});

function handleSquirrelEvent(application) {
    if (process.argv.length === 1) {
        return false;
    }
    const ChildProcess = require('child_process');
    const path = require('path');
    const appFolder = path.resolve(process.execPath, '..');
    const rootAtomFolder = path.resolve(appFolder, '..');
    const updateDotExe = path.resolve(path.join(rootAtomFolder, 'Update.exe'));
    const exeName = path.basename(process.execPath);
    const spawn = function (command, args) {
        let spawnedProcess, error;
        try {
            spawnedProcess = ChildProcess.spawn(command, args, {
                detached: true
            });
        } catch (error) { }
        return spawnedProcess;
    };
    const spawnUpdate = function (args) {
        return spawn(updateDotExe, args);
    };
    const squirrelEvent = process.argv[1];
    switch (squirrelEvent) {
        case '--squirrel-install':
        case '--squirrel-updated':
            // Optionally do things such as:
            // - Add your .exe to the PATH
            // - Write to the registry for things like file associations and
            //   explorer context menus
            // Install desktop and start menu shortcuts
            spawnUpdate(['--createShortcut', exeName]);
            setTimeout(application.quit, 1000);
            return true;
        case '--squirrel-uninstall':
            // Undo anything you did in the --squirrel-install and
            // --squirrel-updated handlers
            // Remove desktop and start menu shortcuts
            spawnUpdate(['--removeShortcut', exeName]);
            setTimeout(application.quit, 1000);
            return true;
        case '--squirrel-obsolete':
            // This is called on the outgoing version of your app before
            // we update to the new version - it's the opposite of
            // --squirrel-updated
            application.quit();
            return true;
    }
};

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.