import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuController, LoadingController, ModalController } from '@ionic/angular';
import { AuthenticatorService } from 'src/app/services/authenticatorService/authenticator.service';
import { AlertService } from 'src/app/services/alertService/alert.service';
import { RegisterModalPage } from 'src/app/pages/register-modal/register-modal.page';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})

export class LoginPage implements OnInit {

  email_value: string;
  password_value: string;
  loaderToShow: any;

  constructor(
    private router: Router,
    private authService: AuthenticatorService,
    private alertService: AlertService,
    private menuCtrl: MenuController,
    private loadingCtrl: LoadingController,
    private modalController: ModalController,
  ) { }

  ngOnInit() { }

  ionViewWillEnter() {
    this.menuCtrl.enable(false, 'menu');
  }

  doLogin(form) {
    this.showLoader();
    this.authService.login(form)
      .then(() => {
        this.authService.getStoredMail()
        .then(data => {
          if (data !== null) {
            document.getElementById("email").setAttribute("value", data);
          }
          console.log('Connected to:', data);
        });
        this.menuCtrl.enable(true, 'menu');
        this.loadingCtrl.dismiss();
        this.router.navigate(['home']);
      })
      .catch(error => {
        this.loadingCtrl.dismiss();
        this.alertService.showAlert("Something gone wrong!", error);
      });
  }

  async doRegister() {
    const modalRegister = await this.modalController.create({
      component: RegisterModalPage,
      cssClass: 'modal-register-css',
    });
    return await modalRegister.present();
  }

  private showLoader() {
    this.loaderToShow = this.loadingCtrl.create({
      message: 'Signing in, wait for a response!'
    }).then((res) => {
      res.present();
    });
  }
}
