import { Component, OnInit, HostListener } from '@angular/core';
import { LoadingController, ModalController, NavParams } from '@ionic/angular';
import { ElectronService } from 'ngx-electron';
import { RemoveRendererFabrics } from 'src/app/components/removeRenderer/remove-renderer-fabrics.component';
import { RemoveRendererPaymentTerms } from 'src/app/components/removeRenderer/remove-renderer-paymentterms.component';
import { Customer } from 'src/app/interfaces/customer';
import { Fabric } from 'src/app/interfaces/fabric';
import { Supplier } from 'src/app/interfaces/supplier';
import { AlertService } from 'src/app/services/alertService/alert.service';
import { FabricsService } from 'src/app/services/fabricsService/fabrics.service';
import { PurchaseOrdersService } from 'src/app/services/purchaseOrderService/purchase-orders.service';
import { SuppliersService } from 'src/app/services/supplierService/suppliers.service';

@Component({
  selector: 'app-orders-invoice-modal',
  templateUrl: './orders-invoice-modal.page.html',
  styleUrls: ['./orders-invoice-modal.page.scss'],
})
export class OrdersInvoiceModalPage implements OnInit {

  public frameworkComponents;
  public context;
  private client : Customer;

  vifrex_image: string = "./assets/Vifrex_BCN.png";

  possible: string = "1234567890";
  lengthOfCode: number = 3;

  po_reference: string = "";
  po_date: string;

  public totalrow: number;
  public total_po: number;

  dataForDocuments: Array<any> = [];
  private last_reference: string = "";
  private last_digits: number;
  private year: number;

  loaderToShow: any;

  supplier: Supplier;
  public fabric = <Fabric>{};

  generateDisabled: boolean = false;
  PDFDisabled: boolean = true;
  removeDisabled: boolean = true;

  // Grid Options
  fabricsGrid: any;
  fabricsGridApi: any;
  fabricsGridColumnApi: any;

  fabricsRowData: any = [];
  fabricsColumnDefs = [
    { headerName: "Vifrex code", field: "vifrex_code", autoHeight: true, sortable: true, resizable: true, filter: true, editable: true, cellStyle: { border: '1px solid', color: 'black', textAlign: 'left', 'white-space': 'normal' } },
    { headerName: "Color", field: "color", autoHeight: true, sortable: true, resizable: true, filter: true, editable: true, cellStyle: { border: '1px solid', color: 'black', textAlign: 'left', 'white-space': 'normal' } },
    { headerName: "Length (m)", field: "length", autoHeight: true, sortable: true, resizable: true, filter: true, editable: true, cellStyle: { border: '1px solid', color: 'black', textAlign: 'left', 'white-space': 'normal' } },
    { headerName: "Ex Mill Date", field: "ex_mill_date", autoHeight: true, sortable: true, resizable: true, filter: true, editable: true, cellStyle: { border: '1px solid', color: 'black', textAlign: 'left', 'white-space': 'normal' } },
    { headerName: "ETA", field: "eta", autoHeight: true, sortable: true, resizable: true, filter: true, editable: true, cellStyle: { border: '1px solid', color: 'black', textAlign: 'center', 'white-space': 'normal' } },
    { headerName: "Incoterm", field: "inco_term", autoHeight: true, sortable: true, resizable: true, filter: true, editable: true, cellStyle: { border: '1px solid', color: 'black', textAlign: 'left', 'white-space': 'normal' } },
    { headerName: "Price (€/m)", field: "price", autoHeight: true, sortable: true, resizable: true, filter: true, editable: true, cellStyle: { border: '1px solid', color: 'black', textAlign: 'center', 'white-space': 'normal' }, },
    { headerName: "Total", field: "total", autoHeight: true, sortable: true, resizable: true, filter: true, cellStyle: { border: '1px solid', color: 'black', textAlign: 'center', 'white-space': 'normal' }, },
    { headerName: "", field: "remove", cellRenderer: "removeRendererFabric", width: 60 }
  ];

  summaryGrid: any;
  summaryGridApi: any;
  summaryGridColumnApi: any;

  summaryRowData: any = [];
  summaryColumnDefs = [
    { headerName: "Comments", field: "comments", autoHeight: true, sortable: true, filter: true, editable: true, cellStyle: { border: '1px solid', color: 'black', textAlign: 'left' } },
    { headerName: "Total", field: "po_total", autoHeight: true, width: 40, sortable: true, filter: true, cellStyle: { border: '1px solid', color: 'black', textAlign: 'center', 'white-space': 'normal' }, },
  ];

  paymentTermsGrid: any;
  paymentTermsGridApi: any;
  paymentTermsGridColumnApi: any;

  paymentTermsRowData: any = [];
  paymentTermsColumnDefs = [
    { headerName: "Payment terms information", field: "payment_term_entry", sortable: true, filter: true, editable: true, cellStyle: { border: '1px solid', color: 'black', textAlign: 'left' } },
    { headerName: "", field: "remove", cellRenderer: "removeRendererPaymentTerms", width: 20 }
  ];

  constructor(
    private navparams: NavParams,
    private supplierService: SuppliersService,
    private fabricService: FabricsService,
    private alertService: AlertService,
    private loadingCtrl: LoadingController,
    private purchaseOrderService: PurchaseOrdersService,
    private electronService: ElectronService,
    private modalController: ModalController,
  ) {
    this.frameworkComponents = {
      removeRendererPaymentTerms: RemoveRendererPaymentTerms,
      removeRendererFabric: RemoveRendererFabrics

    };
    this.context = { componentParent: this };
  }

  ngOnInit() {
    if (this.navparams.get("isNewPO")) {
      this.client = {
        "company": this.navparams.get("company"),
        "name": this.navparams.get("client"),
        "department": this.navparams.get("department"),
        "email": '',
        "phone": '',
      }
      this.removeDisabled = true;
      if (this.navparams.get("po_type") == 22) {
        this.vifrex_image = "./assets/Vifrex_International.png";
      }
      this.createReference();
      this.po_date = this.get_current_date();
      this.supplierService.getSupplier(this.navparams.get("supplier")).subscribe(sup => {
        if (sup != null) {
          this.supplier = <Supplier>sup;
        } else {
          this.alertService.showAlert("Purchase Order problem", "Supplier not found in database, check connection.");
          this.modalController.dismiss();
        }
      });
      this.fabricService.getFabric(this.navparams.get("vifrex_code")).subscribe(fab => {
        if (fab != null) {
          this.fabric = <Fabric>fab;
          this.fabric.code = fab["fabric_supplier_code"];
          this.fabric.vx = fab["vifrex_id"];
        } else {
          this.alertService.showAlert("Purchase Order problem", "Fabric not found in database, check connection.");
          this.modalController.dismiss();
        }
      })
    } else {
      this.removeDisabled = false;
      this.po_reference = this.navparams.get("reference");
      if (parseInt(this.po_reference.substring(8, 10)) == 22) {
        this.vifrex_image = "./assets/Vifrex_International.png";
      }
      this.po_date = this.navparams.get("date");
      this.supplierService.getSupplier(this.navparams.get("supplier")).subscribe(sup => {
        if (sup != null) {
          this.supplier = <Supplier>sup;
        } else {
          this.alertService.showAlert("Purchase Order problem", "Supplier not found in database, check connection.");
          this.modalController.dismiss();
        }
      });
      this.obtainPurchaseOrderEntries();
    }
  }

  private obtainPurchaseOrderEntries() {
    this.purchaseOrderService.getPurchaseOrderEntries(this.po_reference).subscribe(data => {
      this.fabricsRowData = data[0];

      this.paymentTermsRowData = data[1];

      this.summaryRowData = data[2];

      this.fabricService.getFabric(this.fabricsRowData[0]["vifrex_code"]).subscribe(fab => {
        if (fab != null) {
          this.fabric = <Fabric>fab;
          this.fabric.code = fab["fabric_supplier_code"];
          this.fabric.vx = fab["vifrex_id"];
        } else {
          this.alertService.showAlert("Purchase Order problem", "Fabric not found in database, check connection.");
          this.modalController.dismiss();
        }
      });

    }, error => {
      this.alertService.showAlert("Purchase Order problem", "Something went wrong obtaining the order entries.")
      this.modalController.dismiss();
    })
  }

  private createReference() {
    const today = new Date();
    const mm = String(today.getMonth() + 1).padStart(2, '0'); //Enero es 0
    const yyyy = today.getFullYear();

    this.po_reference = 'PO' + yyyy + mm + this.navparams.get("po_type") + 0 + 0 + 1;

    this.purchaseOrderService.getLastOrder(yyyy, mm, this.navparams.get("po_type")).subscribe((order: any[]) => {
      if (order.length > 0) {
        this.last_reference = order[0]['reference'];
        this.last_digits = parseInt(this.last_reference.substr(this.last_reference.length - 3))
        this.year = parseInt(this.last_reference.substring(2, 6));
        if (yyyy == this.year) {
          if (this.last_digits < 9) {
            this.last_digits = this.last_digits + 1;
            this.po_reference = 'PO' + yyyy + mm + this.navparams.get("po_type") + 0 + 0 + this.last_digits;
          } else if (this.last_digits < 99) {
            this.last_digits = this.last_digits + 1;
            this.po_reference = 'PO' + yyyy + mm + this.navparams.get("po_type") + 0 + this.last_digits;
          } else {
            this.last_digits = this.last_digits + 1;
            this.po_reference = 'PO' + yyyy + mm + this.navparams.get("po_type") + this.last_digits;
          }
        }
      }
    });
  }

  private get_current_date(): string {
    const today = new Date();
    const dd = String(today.getDate()).padStart(2, '0');
    const mm = String(today.getMonth() + 1).padStart(2, '0'); //Enero es 0
    const yyyy = today.getFullYear();

    return dd + "/" + mm + "/" + yyyy;
  }

  public addRow(rowType) {
    if (rowType === "fabric") {
      this.fabricsRowData.push({ vifrex_code: this.fabric.vx, color: '', length: '0', ex_mill_date: '', eta: '', inco_term: '', price: '0', total: '0' });
      this.fabricsGridApi.setRowData(this.fabricsRowData);
    } else if (rowType === "paymentTerm") {
      this.paymentTermsRowData.push({ payment_term_entry: '' });
      this.paymentTermsGridApi.setRowData(this.paymentTermsRowData);
    }
    this.sizeToFit();
  }

  public generatePurchaseOrder() {
    this.generateDisabled = true;
    this.alertService.alertToConfirm("Purchase Order creation", 'Do you want to save the purchase order: #' + this.po_reference + ' in the database?').then(() => {
      this.showLoader('Saving purchase order in database...');

      if (this.navparams.get("isNewPO")) {
        this.purchaseOrderService.postOrder(this.po_reference, this.supplier.supplier_code, this.po_date, this.client, this.summaryRowData[0].comments, this.fabricsRowData, this.paymentTermsRowData).then(resp => {

          this.alertService.doToast(resp);
          this.loadingCtrl.dismiss();
          this.PDFDisabled = false;
          this.generateDisabled = false;

        }, error => {
          this.loadingCtrl.dismiss()
          this.alertService.showAlert("Database Error", 'Connection with database is broken, check the server status.');
        });
      } else {
        this.purchaseOrderService.modifyOrder(this.po_reference, this.fabricsRowData, this.summaryRowData[0].comments, this.paymentTermsRowData).then(resp => {

          this.alertService.doToast(resp)
          this.loadingCtrl.dismiss();
          this.PDFDisabled = false;
          this.generateDisabled = false;

        }, error => {
          this.loadingCtrl.dismiss()
          this.alertService.showAlert("Database Error", 'Connection with database is broken, check the server status.');
        });
      }

    }, error => {
      this.loadingCtrl.dismiss()
      this.alertService.showAlert("Error", 'Internal error');
    });
  }

  public removePurchaseOrder() {
    this.alertService.alertToConfirm("Are you sure?", "You can't recover this purchase order after being erased").then(() => {
      this.purchaseOrderService.removeOrder(this.po_reference).then(() => {
        this.alertService.doToast("Purchase Order succesfully removed");
        this.modalController.dismiss();
      }).catch(error => {
        this.alertService.showAlert("Database Error", 'Connection with database is broken, check the server status.');
      })
    })
  }

  public generatePDF() {
    this.PDFDisabled = true;
    let imData = [];

    if (parseInt(this.po_reference.substring(8, 10)) == 22) {
      let imageData = {
        "image_string": '/assets/VX_International_info.png'
      }
      imData.push(imageData);
      this.dataForDocuments.push(imData)
    } else if (parseInt(this.po_reference.substring(8, 10)) == 11) {
      let imageData = {
        "image_string": '/assets/VX_BCN_info.png'
      }
      imData.push(imageData);
      this.dataForDocuments.push(imData)
    }


    let arrayData = [];
    let postData = {
      "po_reference": this.po_reference,
      "po_date": this.po_date,
      "supplier_name": this.supplier.supplier_name,
      "supplier_code": this.supplier.supplier_code,
      "country": this.supplier.country,
      "address": this.supplier.address,
      "phone_number": this.supplier.phone_number,
      "fax": this.supplier.fax,
      "contact_person": this.supplier.contact_person,
      "postal_code": this.supplier.postal_code,
      "vifrex_code": this.fabric.vx,
      "supplier": this.fabric.supplier,
      "fabric_supplier_code": this.fabric.code,
      "composition": this.fabric.composition,
      "construction": this.fabric.construction,
      "width": this.fabric.width,
      "weight": this.fabric.weight,
      "type": this.fabric.type
    }
    arrayData.push(postData)

    this.dataForDocuments.push(arrayData)

    let fabricsArray = [];
    this.fabricsRowData.forEach(element => {
      let Data = {
        "vifrex_code": element.vifrex_code,
        "color": element.color,
        "length": element.length,
        "date_ex_mill": element.ex_mill_date,
        "eta": element.eta,
        "price": element.price,
        "inco_term": element.inco_term,
        "total": element.total,
      }
      fabricsArray.push(Data);
    });

    this.dataForDocuments.push(fabricsArray)

    let paymentTermsArray = [];
    this.paymentTermsRowData.forEach(element => {
      console.log(element);
      let Data = {
        "payment_term_entry": element.payment_term_entry
      }
      paymentTermsArray.push(Data);
    });

    this.dataForDocuments.push(paymentTermsArray)

    let summaryArray = [];
    this.summaryRowData.forEach(element => {
      console.log(element);
      let Data = {
        "comments": element.comments,
        "total": element.po_total,
      }
      summaryArray.push(Data);
    });

    this.dataForDocuments.push(summaryArray)

    this.alertService.doToast(this.electronService.ipcRenderer.sendSync("PrintPurchaseOrder", this.dataForDocuments));
    this.PDFDisabled = false;
  }

  private showLoader(message) {
    this.loaderToShow = this.loadingCtrl.create({
      message: message
    }).then((res) => {
      res.present();
    });
  }

  public calculateTotal() {
    this.total_po = 0;
    this.fabricsRowData.forEach(row => {
      this.totalrow = 0;
      this.totalrow = parseFloat(row.length) * parseFloat(row.price);
      row.total = this.totalrow.toFixed(2);
      this.total_po = this.total_po + parseFloat(row.length) * parseFloat(row.price);
    });

    this.summaryRowData.forEach(element => {
      element.po_total = this.total_po.toFixed(2);
    });

    this.fabricsGridApi.setRowData(this.fabricsRowData);
    this.summaryGridApi.setRowData(this.summaryRowData);

    this.sizeToFit();
  }

  public onCellValueChanged(event) {
    this.sizeToFit();
    this.calculateTotal();
  }

  sizeToFit() {
    this.fabricsGridApi.sizeColumnsToFit();
    this.fabricsGridApi.resetRowHeights();
    this.paymentTermsGridApi.sizeColumnsToFit();
    this.summaryGridApi.sizeColumnsToFit();
    this.summaryGridApi.resetRowHeights();
  }

  autoSizeAll() {
    var allColumnIds = [];
    this.fabricsGridColumnApi.getAllColumns().forEach(function (column) {
      allColumnIds.push(column.colId);
    });
    this.fabricsGridColumnApi.autoSizeColumns(allColumnIds, true);

    this.paymentTermsGridColumnApi.getAllColumns().forEach(function (column) {
      allColumnIds.push(column.colId);
    });
    this.paymentTermsGridColumnApi.autoSizeColumns(allColumnIds, true);

    this.summaryGridColumnApi.getAllColumns().forEach(function (column) {
      allColumnIds.push(column.colId);
    });
    this.summaryGridColumnApi.autoSizeColumns(allColumnIds, true);
  }

  fabricsOnGridReady(grid, params) {
    this.fabricsGrid = grid;
    this.fabricsGridApi = params.api; // To access the grids API
    this.fabricsGridColumnApi = params.columnApi;

    if (this.navparams.get("isNewPO")) {
      this.fabricsRowData.push({ vifrex_code: this.navparams.get("vifrex_code"), color: '', length: '0', ex_mill_date: '', eta: '', inco_term: '', price: '0', total: '0' });
      this.fabricsGridApi.setRowData(this.fabricsRowData);
    } else {
      this.fabricsGridApi.setRowData(this.fabricsRowData);
      this.calculateTotal();
    }
  }

  summaryOnGridReady(grid, params) {
    this.summaryGrid = grid;
    this.summaryGridApi = params.api; // To access the grids API
    this.summaryGridColumnApi = params.columnApi;

    if (this.navparams.get("isNewPO")) {
      this.summaryRowData.push({ comments: '', po_total: '0' });
      this.summaryGridApi.setRowData(this.summaryRowData);
    } else {
      this.summaryGridApi.setRowData(this.summaryRowData);
    }
  }

  paymentTermsOnGridReady(grid, params) {
    this.paymentTermsGrid = grid;
    this.paymentTermsGridApi = params.api; // To access the grids API
    this.paymentTermsGridColumnApi = params.columnApi;

    if (!this.navparams.get("isNewPO")) {
      this.paymentTermsGridApi.setRowData(this.paymentTermsRowData);
    }

    this.sizeToFit();
  }

  public removeMethodPaymentTerms(cell) {
    this.paymentTermsRowData.splice(cell, 1)
    this.paymentTermsGridApi.setRowData(this.paymentTermsRowData);
  }

  public removeMethodFabrics(cell) {
    this.fabricsRowData.splice(cell, 1)
    this.fabricsGridApi.setRowData(this.fabricsRowData);
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.sizeToFit();
  }

}
