import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../../shared.module'

import { IonicModule } from '@ionic/angular';

import { OrdersInvoiceModalPage } from './orders-invoice-modal.page';

import { AgGridModule } from 'ag-grid-angular';

import { RemoveRendererFabrics } from 'src/app/components/removeRenderer/remove-renderer-fabrics.component';

const routes: Routes = [
  {
    path: '',
    component: OrdersInvoiceModalPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    RouterModule.forChild(routes),
    AgGridModule.withComponents([RemoveRendererFabrics]),
  ],
  declarations: [OrdersInvoiceModalPage]
})
export class OrdersInvoiceModalPageModule {}
