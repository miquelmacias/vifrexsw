import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdersInvoiceModalPage } from './orders-invoice-modal.page';

describe('OrdersInvoiceModalPage', () => {
  let component: OrdersInvoiceModalPage;
  let fixture: ComponentFixture<OrdersInvoiceModalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrdersInvoiceModalPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrdersInvoiceModalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
