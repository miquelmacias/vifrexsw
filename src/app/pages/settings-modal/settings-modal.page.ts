import { Component, OnInit } from '@angular/core';
import { LoadingController, ModalController } from '@ionic/angular';
import { AuthenticatorService } from 'src/app/services/authenticatorService/authenticator.service';
import { AlertService } from 'src/app/services/alertService/alert.service';

@Component({
  selector: 'app-settings-modal',
  templateUrl: './settings-modal.page.html',
  styleUrls: ['./settings-modal.page.scss'],
})
export class SettingsModalPage implements OnInit {

  private loaderToShow: Promise<any>;
  password: any;
  password2: any;

  constructor(
    private authService: AuthenticatorService,
    private alertService: AlertService,
    private loadingCtrl: LoadingController,
    private modalController: ModalController,
  ) { }

  ngOnInit() {
  }

  send(form) {
    if (form.value.password != form.value.password2) {
      this.alertService.showAlert('Both of your password fields must match.', '');
      return;
    }
    this.showLoader();
    this.authService.updatePassword(form.value.password)
      .then(() => {
        this.loadingCtrl.dismiss();
        this.alertService.showAlert("Your password was updated!", "");
        this.modalController.dismiss();
      })
      .catch(error => {
        this.loadingCtrl.dismiss();
        this.alertService.showAlert('We couldn\'t update your password', error);
      });
  }

  private showLoader() {
    this.loaderToShow = this.loadingCtrl.create({
      message: 'Waiting for a response from the server...'
    }).then((res) => {
      res.present();
    });
  }

}
