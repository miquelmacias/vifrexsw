import { Component, OnInit } from '@angular/core';
import { LoadingController, ModalController } from '@ionic/angular';
import { AuthenticatorService } from 'src/app/services/authenticatorService/authenticator.service';
import { AlertService } from 'src/app/services/alertService/alert.service';

@Component({
  selector: 'app-register-modal',
  templateUrl: './register-modal.page.html',
  styleUrls: ['./register-modal.page.scss'],
})
export class RegisterModalPage implements OnInit {

  private loaderToShow: Promise<any>;
  public name: string = '';
  public email: string = '';
  public password: string = '';
  public password2: string = '';

  constructor(
    private authService: AuthenticatorService,
    private alertService: AlertService,
    private loadingCtrl: LoadingController,
    private modalController: ModalController,
  ) { }

  ngOnInit() {
  }

  sendRegister(form) {
    if (form.value.password != form.value.password2) {
      this.alertService.showAlert('Both of your password fields must match.', ">:/");
      return;
    }
    this.showLoader();
    this.authService.register(form)
      .then(() => {
        this.loadingCtrl.dismiss();
        this.alertService.showAlert("You have been successfully registered!", "");
        this.modalController.dismiss();
      })
      .catch(error => {
        this.loadingCtrl.dismiss();
        this.alertService.showAlert('Something gone wrong!', error);
      });
  }

  private showLoader() {
    this.loaderToShow = this.loadingCtrl.create({
      message: 'Waiting for a response from the server...'
    }).then((res) => {
      res.present();
    });
  }

}
