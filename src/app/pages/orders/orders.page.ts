import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

import { SearchService } from "src/app/services/searchService/search.service";
// import { AlertService } from 'src/app/services/alertService/alert.service';
import { OrdersModalPage } from '../orders-modal/orders-modal.page';

import { Customer } from 'src/app/interfaces/customer';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.page.html',
  styleUrls: ['./orders.page.scss'],
})
export class OrdersPage implements OnInit {
  public searchField: string;
  public customers: Array<Customer>;
  public filtered_customers: Array<Customer>;
  public selection: Customer;
  public filter_field: string;
  public disabledValue: boolean = false;
  public loading: boolean = false;

  constructor(
    private modalController: ModalController,
    // private alertService: AlertService,
    private searchService: SearchService,
  ) { }

  ngOnInit() {
    this.refresh();
  }

  public refresh(): Promise<unknown> {
    this.loading = true;
    return new Promise((resolve, reject) => {
      this.searchService
        .searchAll('customer')
        .subscribe(data => {
          // this.alertService.doToast("Showing received data");
          this.customers = <Array<Customer>>data;
          this.filtered_customers = <Array<Customer>>data;
          this.loading = false;
          resolve(true);
        }, error => {
          this.loading = false;
          reject(error);
        });
    });
  }

  public filter(): void {
    this.filtered_customers = this.doFilter(this.searchField);
  }

  private doFilter(searchTerm) {
    if (searchTerm == "") { return this.customers }
    return this.customers.filter(item => {
      return item.name.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
    });
  }

  async createSPO() {
    this.disabledValue = true;
    let modalOrder = await this.modalController.create({
      component: OrdersModalPage,
      componentProps: {
        "client": this.selection,
        "isNewSPO": true,
        "reference": ""
      },
      cssClass: 'modal-common-css'
    });
    
    modalOrder.onDidDismiss().then(() => {
      this.disabledValue = false;
      modalOrder = null;
    });

    return await modalOrder.present();
  }

}
