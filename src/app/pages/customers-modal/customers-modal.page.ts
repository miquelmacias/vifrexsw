import { Component, OnInit } from '@angular/core';
import { NavParams } from '@ionic/angular';
import { Customer } from '../../interfaces/customer';

@Component({
  selector: 'app-customers-modal',
  templateUrl: './customers-modal.page.html',
})

export class CustomerModalPage implements OnInit {

  id: string;
  modalCustomer: Customer;
  isThisModal: boolean = true;

  constructor ( private navParams: NavParams, ) 
  {
    this.id = this.navParams.get("id");
    this.modalCustomer = {
      'company': this.navParams.get('Company'),
      'department': this.navParams.get('Department'),
      'name': this.navParams.get('Name'),
      'email': this.navParams.get('Email'),
      'phone': this.navParams.get('Phone'),
    }
    console.log(this.modalCustomer)
  }

  ngOnInit() {
  }
}
