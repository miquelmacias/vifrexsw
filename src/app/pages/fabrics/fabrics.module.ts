import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { NgxQRCodeModule } from 'ngx-qrcode2';
import { SharedModule } from '../../shared.module'
import { FabricsPage } from './fabrics.page';

const routes: Routes = [
  {
    path: '',
    component: FabricsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    RouterModule.forChild(routes),
    NgxQRCodeModule
  ],
  declarations: [FabricsPage]
})
export class FabricsPageModule {}
