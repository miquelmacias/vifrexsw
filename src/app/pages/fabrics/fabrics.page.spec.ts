import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FabricsPage } from './fabrics.page';

describe('FabricsPage', () => {
  let component: FabricsPage;
  let fixture: ComponentFixture<FabricsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FabricsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FabricsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
