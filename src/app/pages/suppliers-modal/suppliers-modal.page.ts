import { Component, OnInit } from '@angular/core';
import { NavParams } from '@ionic/angular';
import { Supplier } from '../../interfaces/supplier';
@Component({
  selector: 'app-suppliers-modal',
  templateUrl: './suppliers-modal.page.html',
})
export class SupplierModalPage implements OnInit {

  id: string;
  modalSupplier: Supplier;
  isThisModal: boolean = true;

  constructor ( private navParams: NavParams, ) {
    this.id = this.navParams.get("id");
    this.modalSupplier = {
      'supplier_name': this.navParams.get('Supplier'),
      'supplier_code': this.navParams.get('SupplierCode'),
      'country': this.navParams.get('Country'),
      'address': this.navParams.get('Address'),
      'phone_number': this.navParams.get('Phone'),
      'fax': this.navParams.get('Fax'),
      'contact_person': this.navParams.get('Contact'),
      'postal_code': this.navParams.get('PostalCode'),
      'VAT': this.navParams.get('Vat'),
    }
  }

  ngOnInit() {
  }

}
