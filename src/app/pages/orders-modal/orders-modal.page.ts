import { Component, OnInit, HostListener } from '@angular/core';
import { NavParams, LoadingController, ModalController } from '@ionic/angular';
import { RemoveRenderer } from './../../components/removeRenderer/remove-renderer.component'

import QRCode from 'qrcode';
import * as domtoimage from 'dom-to-image'

import { Customer } from 'src/app/interfaces/customer';
import { Supplier } from 'src/app/interfaces/supplier';

import { FabricsService } from 'src/app/services/fabricsService/fabrics.service';
import { OrdersService } from 'src/app/services/orderService/orders.service';
import { AlertService } from 'src/app/services/alertService/alert.service';
import { ElectronService } from 'ngx-electron';
import { SuppliersService } from 'src/app/services/supplierService/suppliers.service';

@Component({
  selector: 'app-orders-modal',
  templateUrl: './orders-modal.page.html',
  styleUrls: ['./orders-modal.page.scss'],
})

export class OrdersModalPage implements OnInit {
  client: Customer;
  supplier: Supplier;
  fabricCode: string = "";
  inputFabricText: string;

  loaderToShow: any;

  supplierIdx: Array<any> = [];
  dataForDocuments: Array<any> = [];
  supplierArray: Array<any> = [];

  scanning: boolean = false;
  stopScanDisabled: boolean = true;
  scanDisabled: boolean = false;
  generateDisabled: boolean = true;
  downloadHidden: boolean = true;
  removeHidden: boolean = true;
  inputHidden: boolean = false;
  canDownloadOrder: boolean = true;

  fabric: any;
  gridApi: any;
  rowData: any = [];

  public frameworkComponents;
  public context;

  qrUrls: Array<string> = [];
  array_of_cards: Array<any> = [];

  possible: string = "1234567890";
  lengthOfCode: number = 6;
  reference = this.makeRandom(this.lengthOfCode, this.possible);
  date: string;

  columnDefs = [
    { headerName: "Vifrex ID", field: "vifrex_id", sortable: true, filter: true },
    { headerName: "Sample Yardage", field: "sampleyardage", sortable: true, filter: true, editable: true },
    { headerName: "Hanger", field: "hanger", sortable: true, filter: true, editable: true },
    { headerName: "Comments Order", field: "comments_order", sortable: true, filter: true, editable: true },
    { headerName: "obId", field: "_id", hide: true },
    { headerName: "Supplier", field: "supplier", hide: true },
    { headerName: "Construction", field: "construction", hide: true },
    { headerName: "Type", field: "type", sortable: true, filter: true, hide: true },
    { headerName: "Fabric Supp. Code", field: "fabric_supplier_code", hide: true },
    { headerName: "Width", field: "width", sortable: true, filter: true, hide: true },
    { headerName: "Finishing", field: "finishing", hide: true },
    { headerName: "Comments", field: "comments", hide: true },
    { headerName: "Weight", field: "weight", sortable: true, filter: true, hide: true },
    { headerName: "Price", field: "price", hide: true },
    { headerName: "Vifrex_Price", field: "vifrex_price", hide: true },
    { headerName: "Date", field: "date", hide: true },
    { headerName: "Fabric Composition", field: "composition", hide: true },
    { headerName: "Fabric Description", field: "fabric_description", hide: true },
    { headerName: "Order date", field: "date_order", hide: true },
    { headerName: "Supplier name", field: "supplier_name", hide: true },
    { headerName: "Reference", field: "reference", hide: true },
    { headerName: "", field: "remove", cellRenderer: "removeRenderer", width: 90 }
  ];

  constructor(
    private fabricsService: FabricsService,
    private navparams: NavParams,
    private orderService: OrdersService,
    private alertService: AlertService,
    private loadingCtrl: LoadingController,
    private electronService: ElectronService,
    private modalController: ModalController,
    private supplierService: SuppliersService
  ) {
    this.frameworkComponents = {
      removeRenderer: RemoveRenderer
    };
    this.context = { componentParent: this };
  }

  ngOnInit() {
    this.fabricCode = "";
    this.client = this.navparams.get("client");
    var month = (new Date().getUTCMonth()) + 1;
    if (this.navparams.get("isNewSPO")) {
      this.reference = month.toString() + new Date().getFullYear().toString().substr(2, 3) + this.makeRandom(this.lengthOfCode, this.possible);
      this.date = this.get_current_date();
      this.orderService.getOrder(this.reference).subscribe(data => {
        if (data != null) {
          this.reference = month.toString() + new Date().getFullYear().toString().substr(2, 3) + this.makeRandom(this.lengthOfCode, this.possible);
        }
      });
    } else {
      this.reference = this.navparams.get("reference")
      this.date = this.navparams.get("date")
      this.obtainOrderEntries();
      this.removeHidden = false;
    }
  }

  addFabricManually() {
    if (this.inputFabricText != undefined && this.inputFabricText !== '') {
      let fabricInit = this.inputFabricText.substring(0, 2);
      if(fabricInit === 'VX') {
        this.addFabricTolist(this.inputFabricText, true);
      } else {
        this.inputFabricText = "VX" + this.inputFabricText;
        this.addFabricTolist(this.inputFabricText, true);
      }

      this.inputFabricText = ""
    } else {
      this.alertService.doToast("Nothing to add.")
    }
  }

  @HostListener('document:keypress', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent) {
    if (event.key === "Enter" && this.scanning == true) {
      this.addFabricTolist(this.fabricCode, false);
    } else if (this.scanning == true) {
      this.fabricCode = this.fabricCode + event.key;
    }
  }

  private addFabricTolist(fabric, manually) {
    this.fabricsService.getFabric(fabric).subscribe(data => {
      if (data != null) {
        this.fabric = data;
        this.supplierService.getSupplier(this.fabric.supplier).subscribe(sup => {
          if (sup != null) {
            this.supplier = <Supplier>sup;
            let nodeData = {
              "vifrex_id": this.fabric.vifrex_id,
              "obId": this.fabric._id,
              "supplier": this.fabric.supplier,
              "construction": this.fabric.construction,
              "fabric_supplier_code": this.fabric.fabric_supplier_code,
              "width": this.fabric.width,
              "finishing": this.fabric.finishing,
              "comments": this.fabric.comments,
              "weight": this.fabric.weight,
              "price": this.fabric.price,
              "vifrex_price": this.fabric.vifrex_price,
              "date": this.fabric.date,
              "composition": this.fabric.composition,
              "fabric_description": this.fabric.fabric_description,
              "sampleyardage": "0",
              "hanger": "1",
              "comments_order": "",
              "supplier_name": this.supplier.supplier_name,
              "date_order": this.date,
              "reference": this.reference
            }
            this.rowData.push(nodeData);
            this.addSupplier();
            this.gridApi.setRowData(this.rowData);
            this.fabricCode = "";
            this.sizeToFit();
            if (manually) {
              this.generateDisabled = false;
            }
          }
        });
      } else {
        this.alertService.doToast("No fabric with this code.")
        this.fabricCode = "";
      }
    }, error => {
      this.alertService.showAlert("Database Error", 'Connection with database is broken, check the server status.')
      this.fabricCode = "";
    })
  }

  generateOrder() {
    this.alertService.alertToConfirm("Order creation", 'Do you want to save the order: #' + this.reference + ' in the database?').then(() => {
      this.showLoader('Saving order in database...');
      if (!this.navparams.get("isNewSPO")) {
        this.orderService.modifyOrder(this.reference, this.rowData).then(resp => {

          this.alertService.doToast(resp)
          this.setup_fabrics();
          this.removeHidden = true;

        }, error => {
          this.loadingCtrl.dismiss()
          this.alertService.showAlert("Database Error", 'Connection with database is broken, check the server status.');
        });
      } else {
        this.orderService.postOrder(this.reference, this.client, this.rowData).then(resp => {

          this.alertService.doToast(resp)
          this.setup_fabrics();
          this.removeHidden = true;

        }, error => {
          this.loadingCtrl.dismiss()
          this.alertService.showAlert("Database Error", 'Connection with database is broken, check the server status.');
        });
      }
    }, error => {
      this.loadingCtrl.dismiss()
      this.alertService.showAlert("Error", 'Internal error');
    });
  }

  public remove() {
    this.alertService.alertToConfirm("Are you sure?", "You can't recover this order after being erased").then(() => {
      this.orderService.removeOrder(this.reference).then(() => {
        this.alertService.doToast("Order succesfully removed");
        this.modalController.dismiss();
      }).catch(error => {
        this.alertService.showAlert("Database Error", 'Connection with database is broken, check the server status.');
      })
    })
  }

  public download() {
    this.canDownloadOrder = false;
    this.downloadHidden = true;
    this.download_method();
  }

  private download_method() {
    this.showLoader('Generating Excels and .PDF files...');

    let dataArray = [];
    this.rowData.forEach(element => {
      let post = {
        "company": this.client.company,
        "department": this.client.department,
        "name": this.client.name,
        "supplier": element.supplier,
        "supplier_name": element.supplier_name,
        "fabric_supplier_code": element.fabric_supplier_code,
        "reference": this.reference,
        "vifrex_id": element.vifrex_id,
        "sampleyardage": element.sampleyardage,
        "hanger": element.hanger,
        "comments_order": element.comments_order,
        "date_order": this.date,
        "obId": element._id,
        "construction": element.construction,
        "width": element.width,
        "finishing": element.finishing,
        "comments": element.comments,
        "weight": element.weight,
        "price": element.price,
        "vifrex_price": element.vifrex_price,
        "date": element.date,
        "composition": element.composition,
        "fabric_description": element.fabric_description,
      }
      dataArray.push(post);
    });

    this.dataForDocuments.push(dataArray)

    this.supplierArray.forEach(supp => {
      let supArray = [];
      dataArray.forEach(element => {
        if (supp === element.supplier) {
          supArray.push(element)
        }
      })
      this.dataForDocuments.push(supArray)
    })

    this.electronService.ipcRenderer.send("vifrexSPO", this.dataForDocuments);

    this.supplierArray.forEach(supp => {
      let supArray = [];
      this.supplierIdx.forEach(element => {
        if (supp === element.supplier) {
          supArray.push(element.Idx)
        }
      })
      this.getCardsPDF(supArray, supp);
    })
  }

  private setup_fabrics() {
    let qrUrls = this.rowData.map(element => {
      const options = {
        width: 1024,
        height: 1024,
        errorCorrectionLevel: 'H',
        type: 'svg',
        quality: 1.0,
        margin: 0,
      };
      return new Promise((resolve, reject) => {
        QRCode.toDataURL(element['vifrex_id'], options, function (err, url) {
          if (err) reject(err);
          resolve(url);
        });
      });
    });

    Promise.all(qrUrls).then(data => {
      data.forEach((qr, idx) => {
        const ret = {
          'vx': this.rowData[idx]['vifrex_id'],
          'supplier': this.rowData[idx]['supplier'],
          'code': this.rowData[idx]['fabric_supplier_code'],
          'composition': this.rowData[idx]['composition'],
          'construction': this.rowData[idx]['construction'],
          'width': this.rowData[idx]['width'],
          'weight': this.rowData[idx]['weight'],
          'type': this.rowData[idx]['type'],
          'comments': this.rowData[idx]['comments'],
          'date': this.rowData[idx]['date'],
          'finishing': this.rowData[idx]['finishing'],
          'price': this.rowData[idx]['price'],
          'vifrex_price': this.rowData[idx]['vifrex_price'],
          'fabric_description': this.rowData[idx]['fabric_description'],
          'vx_logo': qr,
        }
        this.supplierIdx.push({
          'supplier': this.rowData[idx]['supplier'],
          'Idx': idx
        })
        this.array_of_cards[idx] = ret;
        this.loadingCtrl.dismiss();
        this.downloadHidden = false;
        this.generateDisabled = true;
        this.scanDisabled = true;
        this.inputHidden = true;
      });
    });
  }

  private getCardsPDF(arr, supplier) {
    const scale_up = 4;
    const HTMLElements = document.querySelectorAll(".card");

    let promiseAll = arr.map(element => {
      const sourceElement = <HTMLElement>HTMLElements[element].getElementsByClassName('tag')[0];

      return domtoimage
        .toPng(sourceElement, {
          height: sourceElement.offsetHeight * scale_up,
          width: sourceElement.offsetWidth * scale_up,
          style: {
            transform: "scale(" + scale_up + ")",
            transformOrigin: "top left",
            width: sourceElement.offsetWidth + "px",
            height: sourceElement.offsetHeight + "px",
          }
        }).then(dataUrl => {
          let imgData = new Image();
          imgData.src = dataUrl;
          return imgData;
        });
    })

    Promise.all(promiseAll).then(image_array => {
      this.electronService.ipcRenderer.sendSync("Supplier", supplier);
      image_array.forEach((imgData: HTMLImageElement, idx) => {
        this.electronService.ipcRenderer.sendSync("Cards", imgData.src);
      });
    }).then(() => {
      this.electronService.ipcRenderer.sendSync("PrintPDF", "");
      this.loadingCtrl.dismiss();
      this.modalController.dismiss();
    }).catch(error => {
      this.loadingCtrl.dismiss();
      this.alertService.showAlert("Error creating PDF of tags", "Open order in browser and try to download documents again.");
    });
  }

  private obtainOrderEntries() {
    this.showLoader('Obtaining order from database...');
    this.orderService.getOrderEntries(this.reference).subscribe(data => {
      this.rowData = data;
      this.rowData.forEach(element => {
        this.supplierService.getSupplier(element.supplier).subscribe(sup => {
          if (sup != null) {
            this.supplier = <Supplier>sup;
            element.supplier_name = this.supplier.supplier_name;
            this.addSupplier();
          }
        })
      })
      this.gridApi.setRowData(this.rowData);
      this.sizeToFit();
      this.generateDisabled = false;
      this.loadingCtrl.dismiss();
    }, error => {
      this.alertService.showAlert("Order problem", "Something went wrong obtaining the order entries.")
      this.loadingCtrl.dismiss();
    })
  }

  private addSupplier() {
    if (this.supplierArray.length >= 1) {
      let added = false;
      this.supplierArray.forEach(sup => {
        if (sup === this.supplier.supplier_code) {
          added = true;
        }
      })
      if (!added) {
        this.supplierArray.push(this.supplier.supplier_code);
      }
    } else {
      this.supplierArray.push(this.supplier.supplier_code)
    }
  }

  public scan() {
    this.alertService.doToast("Start scanning vifrex fabrics.")
    this.scanning = true;
    this.stopScanDisabled = false;
    this.scanDisabled = true;
    this.generateDisabled = true;
    this.inputHidden = true;

  }

  public stopScan() {
    this.scanning = false;
    this.stopScanDisabled = true;
    this.scanDisabled = false;
    this.inputHidden = false;

    if (this.rowData.length > 0) {
      this.generateDisabled = false;
    }
  }

  public removeMethod(cell) {
    this.rowData.splice(cell, 1)
    this.gridApi.setRowData(this.rowData);
    if (this.rowData.length == 0) {
      this.generateDisabled = true;
    }
  }

  private sizeToFit() {
    this.gridApi.sizeColumnsToFit();
  }

  public onGridReady(params) {
    this.gridApi = params.api; // To access the grids API
  }

  private makeRandom(lengthOfCode: number, possible: string): string {
    let text = "";
    for (let i = 0; i < lengthOfCode; i++) {
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    }

    return text;
  }

  private showLoader(message) {
    this.loaderToShow = this.loadingCtrl.create({
      message: message
    }).then((res) => {
      res.present();
    });
  }

  private get_current_date(): string {
    const today = new Date();
    const dd = String(today.getDate()).padStart(2, '0');
    const mm = String(today.getMonth() + 1).padStart(2, '0'); //Enero es 0
    const yyyy = today.getFullYear();

    return yyyy + '/' + mm + '/' + dd;
  }
}
