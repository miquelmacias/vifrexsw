import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { OrdersModalPage } from './orders-modal.page';

import { RemoveRenderer } from './../../components/removeRenderer/remove-renderer.component'

import { AgGridModule } from 'ag-grid-angular';
import { SharedModule } from 'src/app/shared.module';

const routes: Routes = [
  {
    path: '',
    component: OrdersModalPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    SharedModule,
    AgGridModule.withComponents([RemoveRenderer]),
  ],
  declarations: [OrdersModalPage]
})
export class OrdersModalPageModule {}
