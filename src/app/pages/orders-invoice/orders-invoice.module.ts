import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../../shared.module'

import { IonicModule } from '@ionic/angular';

import { OrdersInvoicePage } from './orders-invoice.page';

const routes: Routes = [
  {
    path: '',
    component: OrdersInvoicePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    RouterModule.forChild(routes),
  ],
  declarations: [OrdersInvoicePage]
})
export class OrdersInvoicePageModule {}
