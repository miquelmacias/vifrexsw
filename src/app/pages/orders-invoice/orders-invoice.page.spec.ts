import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdersInvoicePage } from './orders-invoice.page';

describe('OrdersInvoicePage', () => {
  let component: OrdersInvoicePage;
  let fixture: ComponentFixture<OrdersInvoicePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrdersInvoicePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrdersInvoicePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
