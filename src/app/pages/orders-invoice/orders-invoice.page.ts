import { Component, OnInit, ViewChild } from '@angular/core';

import { IonSelect, ModalController } from '@ionic/angular';
import { FinalOrder } from 'src/app/interfaces/finalorder';
import { AlertService } from 'src/app/services/alertService/alert.service';
import { FabricsService } from 'src/app/services/fabricsService/fabrics.service';
import { OrdersInvoiceModalPage } from '../orders-invoice-modal/orders-invoice-modal.page';

import { Fabric } from "../../interfaces/fabric";
import { SearchService } from 'src/app/services/searchService/search.service';
import { Customer } from 'src/app/interfaces/customer';

@Component({
  selector: 'app-orders-invoice',
  templateUrl: './orders-invoice.page.html',
  styleUrls: ['./orders-invoice.page.scss'],
})
  // refer to the select via the template reference

export class OrdersInvoicePage implements OnInit {

  @ViewChild('Selector', { static: false }) selector: IonSelect;

  code: string;
  reference: string;
  client: Customer;
  public POs: Array<FinalOrder>;
  public filtered_POs: Array<FinalOrder>;
  public loading: boolean = false;
  public selection: FinalOrder;
  public customers: Array<Customer>;

  fabric: Fabric;

  constructor(
    private fabricsService: FabricsService,
    private modalController: ModalController,
    private alertService: AlertService,
    private searchService: SearchService,
  ) { }

  ngOnInit() {
    this.refresh();
  }

  onChange(input_kind) {
    this.client = input_kind;
  }

  public refresh(): Promise<unknown> {
    this.loading = true;
    return new Promise((resolve, reject) => {
      this.searchService
        .searchAll('finalOrder')
        .subscribe(data => {

          this.POs = <Array<FinalOrder>>data;
          this.filtered_POs = <Array<FinalOrder>>data;

          this.searchService.searchAll("customer").subscribe(data => {
            this.customers = <Array<Customer>>data;
            this.loading = false;
            resolve(true);
          }, error => {
            this.loading = false;
            reject(error);
          })

        }, error => {
          this.loading = false;
          reject(error);
        });
    });
  }

  public filter(): void {
    this.filtered_POs = this.doFilter(this.reference);
  }

  private doFilter(searchTerm) {
    if (searchTerm == "") { return this.POs }
    return this.POs.filter(item => {
      return item.reference.toUpperCase().indexOf(searchTerm.toUpperCase()) > -1;
    });
  }

  createPOBcn(form) {
    if (this.client != null) {
      this.fabricsService.getFabric(this.code).subscribe(data => {
        if (data != null) {
          this.fabric = <Fabric>data;
          this.openNewPOModal(form, this.fabric.supplier, 11);
        } else {
          this.alertService.doToast("No fabric with this code.")
          form.reset();
        }
      }, error => {
        this.alertService.showAlert("Database Error", 'Connection with database is broken, check the server status.')
      })
    } else {
      this.alertService.doToast("Select a client.")
    }
  }

  createPOInt(form) {
    if (this.client != null) {
      this.fabricsService.getFabric(this.code).subscribe(data => {
        if (data != null) {
          this.fabric = <Fabric>data;
          this.openNewPOModal(form, this.fabric.supplier, 22);
        } else {
          this.alertService.doToast("No fabric with this code.")
          form.reset();
        }
      }, error => {
        this.alertService.showAlert("Database Error", 'Connection with database is broken, check the server status.')
      })
    } else {
      this.alertService.doToast("Select a client.")
    }
  }

  async openNewPOModal(form, supplier, type) {
    let modalOrder = await this.modalController.create({
      component: OrdersInvoiceModalPage,
      componentProps: {
        "isNewPO": true,
        "vifrex_code": this.code,
        "supplier": supplier,
        "client": this.client.name,
        "department": this.client.department,
        "company": this.client.company,
        "po_type": type,
      },
      cssClass: 'modal-common-css'
    });

    modalOrder.onDidDismiss().then(() => {
      modalOrder = null;
      form.reset();
      this.fabric = null;
      this.selector.value = null;
      this.refresh();
    });

    return await modalOrder.present();
  }

  async modifyPO() {
    let modalOrder = await this.modalController.create({
      component: OrdersInvoiceModalPage,
      componentProps: {
        "isNewPO": false,
        "reference": this.selection.reference,
        "supplier": this.selection.supplier_code,
        "date": this.selection.date,
        "client": this.selection.client,
      },
      cssClass: 'modal-common-css'
    });

    modalOrder.onDidDismiss().then(() => {
      modalOrder = null;
      this.selector.value = null;
      this.refresh();
    });

    return await modalOrder.present();
  }

}
