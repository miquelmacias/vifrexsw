import { Component, OnInit } from '@angular/core';
import { NavParams } from '@ionic/angular';
import { Fabric } from '../../interfaces/fabric';
@Component({
  selector: 'app-fabrics-modal',
  templateUrl: './fabrics-modal.page.html',
})
export class FabricsModalPage implements OnInit {

  id: string;
  modalFabric: Fabric;
  isThisModal: boolean = true;

  constructor(
    private navParams: NavParams,
  ) {
    this.id = this.navParams.get("id");
    this.modalFabric = {
      'vx': this.navParams.get('Vifrex_id'),
      'supplier': this.navParams.get('Supplier'),
      'code': this.navParams.get('Fabric_supplier_code'),
      'composition': this.navParams.get('Composition'),
      'construction': this.navParams.get('Construction'),
      'width': this.navParams.get('Width'),
      'weight': this.navParams.get('Weight'),
      'type': this.navParams.get('Type'),
      'comments': this.navParams.get('Comments'),
      'date': this.navParams.get('Date'),
      'finishing': this.navParams.get('Finishing'),
      'price': this.navParams.get('Price'),
      'vifrex_price': this.navParams.get('Vifrex_price'),
      'fabric_description': this.navParams.get('Description'),
    }
  }

  ngOnInit() {
  }

}
