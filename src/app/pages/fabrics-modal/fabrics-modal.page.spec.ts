import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FabricsModalPage } from './fabrics-modal.page';

describe('FabricsModalPage', () => {
  let component: FabricsModalPage;
  let fixture: ComponentFixture<FabricsModalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FabricsModalPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FabricsModalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
