import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', loadChildren: './pages/login/login.module#LoginPageModule' },
  { path: 'home', loadChildren: './pages/home/home.module#HomePageModule'},
  { path: 'fabrics', loadChildren: './pages/fabrics/fabrics.module#FabricsPageModule' },
  { path: 'suppliers', loadChildren: './pages/suppliers/suppliers.module#SuppliersPageModule' },
  { path: 'customers', loadChildren: './pages/customers/customers.module#CustomersPageModule' },
  { path: 'orders', loadChildren: './pages/orders/orders.module#OrdersPageModule' },  { path: 'orders-invoice', loadChildren: './pages/orders-invoice/orders-invoice.module#OrdersInvoicePageModule' },
  { path: 'orders-invoice-modal', loadChildren: './pages/orders-invoice-modal/orders-invoice-modal.module#OrdersInvoiceModalPageModule' },

];

@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })],
  exports: [RouterModule]
})

export class AppRoutingModule {}
