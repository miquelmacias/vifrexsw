import { Component, OnInit, HostListener } from "@angular/core";
import { SearchService } from "src/app/services/searchService/search.service";

import QRCode from 'qrcode';
import * as domtoimage from 'dom-to-image'

import { AlertService } from 'src/app/services/alertService/alert.service';
import { ModalController, LoadingController } from '@ionic/angular';
import { CustomerModalPage } from 'src/app/pages/customers-modal/customers-modal.page';
import { SupplierModalPage } from 'src/app/pages/suppliers-modal/suppliers-modal.page';
import { FabricsModalPage } from 'src/app/pages/fabrics-modal/fabrics-modal.page';
import { OrdersModalPage } from 'src/app/pages/orders-modal/orders-modal.page';
import { Customer } from 'src/app/interfaces/customer';
import { ElectronService } from 'ngx-electron';
import { OrdersInvoiceModalPage } from "src/app/pages/orders-invoice-modal/orders-invoice-modal.page";

@Component({
  selector: "app-browser",
  templateUrl: "./browser.component.html",
  styleUrls: ["./browser.component.scss"]
})
export class BrowserComponent implements OnInit {
  /* Client is de Default kind of request, <ion-select> is also selected
  as Client and Grid Columns are written to get Client Search */
  searchField: string;
  kindSelector: string = "customer";
  placeholder: string = "Search customers by Department"
  rowSelection: string = "multiple";
  pdfDownloadHidden: boolean = true;
  hiddenContainer: boolean = true;

  controlDown: boolean = false;
  shiftDown: boolean = false;

  grid: any;
  gridApi: any;
  gridColumnApi: any;

  loaderToShow: any;
  qrUrls: Array<string> = [];
  array_of_cards: Array<any> = [];
  supplierIdx: Array<any> = [];

  // Grid Options
  rowData: any = [];
  columnDefs = [
    { headerName: "obId", field: "id", hide: true },
    { headerName: "Name", field: "name", sortable: true, filter: true },
    { headerName: "Company", field: "company", sortable: true, filter: true },
    { headerName: "Department", field: "department", sortable: true, filter: true },
    { headerName: "Email", field: "email", sortable: true, filter: true },
    { headerName: "Phone Number", field: "phone_number", sortable: true, filter: true }
  ];

  loading: boolean = false;

  constructor(
    private searchService: SearchService,
    private modalController: ModalController,
    private alertService: AlertService,
    private loadingCtrl: LoadingController,
    private electronService: ElectronService
  ) { }

  ngOnInit() { }

  createPDF() {
    if (this.kindSelector == "fabric") {
      this.showLoader('Creating PDF of selected vifrex tags, this could take a minute or more...');
      this.setup_fabrics();
    }
  }

  private setup_fabrics() {
    this.hiddenContainer = false;
    let qrUrls = this.grid.api.getSelectedRows().map(element => {
      const options = {
        width: 1024,
        height: 1024,
        errorCorrectionLevel: 'H',
        type: 'svg',
        quality: 1.0,
        margin: 0,
      };
      return new Promise((resolve, reject) => {
        QRCode.toDataURL(element['vifrex_id'], options, function (err, url) {
          if (err) reject(err);
          resolve(url);
        });
      });
    });

    Promise.all(qrUrls).then(data => {
      let HTMLELs = data.map((qr, idx) => {
        return new Promise((resolve, reject) => {
          const ret = {
            'vx': this.grid.api.getSelectedRows()[idx]['vifrex_id'],
            'supplier': this.grid.api.getSelectedRows()[idx]['supplier'],
            'code': this.grid.api.getSelectedRows()[idx]['fabric_supplier_code'],
            'composition': this.grid.api.getSelectedRows()[idx]['composition'],
            'construction': this.grid.api.getSelectedRows()[idx]['construction'],
            'width': this.grid.api.getSelectedRows()[idx]['width'],
            'weight': this.grid.api.getSelectedRows()[idx]['weight'],
            'type': this.grid.api.getSelectedRows()[idx]['type'],
            'comments': this.grid.api.getSelectedRows()[idx]['comments'],
            'date': this.grid.api.getSelectedRows()[idx]['date'],
            'finishing': this.grid.api.getSelectedRows()[idx]['finishing'],
            'price': this.grid.api.getSelectedRows()[idx]['price'],
            'vifrex_price': this.grid.api.getSelectedRows()[idx]['vifrex_price'],
            'fabric_description': this.grid.api.getSelectedRows()[idx]['fabric_description'],
            'vx_logo': qr
          }
          this.supplierIdx.push({
            'supplier': this.grid.api.getSelectedRows()[idx]['supplier'],
            'Idx': idx
          })
          this.array_of_cards[idx] = ret;
          resolve(ret);
        });
      });

      Promise.all(HTMLELs).then(() => {
        setTimeout(() => {
          this.getCardsPDF();
        }, 2000)
      })
    });
  }

  private getCardsPDF() {
    const scale_up = 4;
    const HTMLElements = document.querySelectorAll(".card");

    let promiseAll = this.supplierIdx.map(element => {
      const sourceElement = <HTMLElement>HTMLElements[element['Idx']].getElementsByClassName('tag')[0];


      return domtoimage
        .toPng(sourceElement, {
          height: sourceElement.offsetHeight * scale_up,
          width: sourceElement.offsetWidth * scale_up,
          style: {
            transform: "scale(" + scale_up + ")",
            transformOrigin: "top left",
            width: sourceElement.offsetWidth + "px",
            height: sourceElement.offsetHeight + "px",
          }
        }).then(dataUrl => {
          let imgData = new Image();
          console.log("created image");
          imgData.src = dataUrl;
          return imgData;
        });
    })

    Promise.all(promiseAll).then(image_array => {
      this.electronService.ipcRenderer.sendSync("InitDoc", "");
      image_array.forEach((imgData: HTMLImageElement, idx) => {
        this.electronService.ipcRenderer.sendSync("Cards", imgData.src);
      });
    }).then(() => {
      let date = this.get_current_date();
      let fileName: string = date + '_Fabrics.pdf';
      this.electronService.ipcRenderer.sendSync("PrintSelectedCards", fileName);
      this.supplierIdx = [];
      this.array_of_cards = [];
      this.hiddenContainer = true;
      this.loadingCtrl.dismiss();
    }).catch(error => {
      this.loadingCtrl.dismiss();
      this.alertService.showAlert("Error creating PDF of tags", "Select fabrics again and try to generate PDF.");
    });
  }

  private get_current_date(): string {
    const today = new Date();
    const dd = String(today.getDate()).padStart(2, '0');
    const mm = today.getMinutes();
    const ss = today.getSeconds();

    return dd + '_' + mm + '_' + ss;
  }


  onSearch() {
    if (this.kindSelector === undefined) {
      this.alertService.doToast("You have to select what are you searching for");
      return;
    }
    if (this.searchField === undefined || this.searchField === "") {
      this.alertService.doToast("You have to write something on the search field");
      return;
    }

    this.loading = true;
    this.searchService
      .search(this.searchField, this.kindSelector)
      .subscribe(data => {
        this.rowData = data;
        if (this.kindSelector == "customer" || this.kindSelector == 'order') {
          this.sizeToFit();
        }
        this.loading = false;
      }, error => {
        this.alertService.showAlert('Database Error', 'Connection with database is broken, check the server status.');
      });
  }

  @HostListener('document:keydown', ['$event'])
  handleKeyboardDownEvent(event: KeyboardEvent) {
    if (event.key === "Shift") {
      this.shiftDown = true;
    } else if (event.key === "Control") {
      this.controlDown = true;
    }
  }

  @HostListener('document:keyup', ['$event'])
  handleKeyboardUpEvent(event: KeyboardEvent) {
    if (event.key === "Shift") {
      this.shiftDown = false;
    } else if (event.key === "Control") {
      this.controlDown = false;
    }
  }

  async onSelectionChanged(grid) {
    if (!this.shiftDown && !this.controlDown) {
      this.pdfDownloadHidden = true;
      switch (this.kindSelector) {
        case "customer":
          const modalCustomer = await this.modalController.create({
            component: CustomerModalPage,
            componentProps: {
              "id": grid.api.getSelectedRows()[0].id,
              'Company': grid.api.getSelectedRows()[0].company,
              'Department': grid.api.getSelectedRows()[0].department,
              'Name': grid.api.getSelectedRows()[0].name,
              'Email': grid.api.getSelectedRows()[0].email,
              'Phone': grid.api.getSelectedRows()[0].phone_number
            },
            cssClass: 'modal-customer-css'
          });
          modalCustomer.onDidDismiss().then(() => {
            this.reloadAGGrid();
          })
          return await modalCustomer.present();

        case "supplier":
          const modalSupplier = await this.modalController.create({
            component: SupplierModalPage,
            componentProps: {
              "id": grid.api.getSelectedRows()[0].id,
              'Supplier': grid.api.getSelectedRows()[0].supplier_name,
              'SupplierCode': grid.api.getSelectedRows()[0].supplier_code,
              'Country': grid.api.getSelectedRows()[0].country,
              'Address': grid.api.getSelectedRows()[0].address,
              'Phone': grid.api.getSelectedRows()[0].phone_number,
              'Fax': grid.api.getSelectedRows()[0].fax,
              'Contact': grid.api.getSelectedRows()[0].contact_person,
              'PostalCode': grid.api.getSelectedRows()[0].postal_code,
              'Vat': grid.api.getSelectedRows()[0].VAT
            },
            cssClass: 'modal-supplier-css'
          });

          modalSupplier.onDidDismiss().then(() => {
            this.reloadAGGrid();
          })
          return await modalSupplier.present();

        case "fabric":
          const modalFabric = await this.modalController.create({
            component: FabricsModalPage,
            componentProps: {
              "id": grid.api.getSelectedRows()[0].id,
              "Supplier": grid.api.getSelectedRows()[0].supplier,
              "Construction": grid.api.getSelectedRows()[0].construction,
              "Type": grid.api.getSelectedRows()[0].type,
              "Fabric_supplier_code": grid.api.getSelectedRows()[0].fabric_supplier_code,
              "Width": grid.api.getSelectedRows()[0].width,
              "Finishing": grid.api.getSelectedRows()[0].finishing,
              "Comments": grid.api.getSelectedRows()[0].comments,
              "Composition": grid.api.getSelectedRows()[0].composition,
              "Weight": grid.api.getSelectedRows()[0].weight,
              "Price": grid.api.getSelectedRows()[0].price,
              "Vifrex_price": grid.api.getSelectedRows()[0].vifrex_price,
              "Date": grid.api.getSelectedRows()[0].date,
              "Vifrex_id": grid.api.getSelectedRows()[0].vifrex_id,
              "Description": grid.api.getSelectedRows()[0].fabric_description
            },
            cssClass: 'modal-fabric-css'
          });

          modalFabric.onDidDismiss().then(() => {
            this.reloadAGGrid();
          })
          return await modalFabric.present();
        case "order":
          let client: Customer = {
            company: grid.api.getSelectedRows()[0].client_company,
            department: grid.api.getSelectedRows()[0].client_department,
            name: grid.api.getSelectedRows()[0].client_name,
            email: "",
            phone: ""
          }

          let modalOrder = await this.modalController.create({
            component: OrdersModalPage,
            componentProps: {
              "date": grid.api.getSelectedRows()[0].date,
              "client": client,
              "isNewSPO": false,
              "reference": grid.api.getSelectedRows()[0].reference
            },
            cssClass: 'modal-common-css'
          });

          modalOrder.onDidDismiss().then(() => {
            this.reloadAGGrid();
          });

          return await modalOrder.present();

        case "finalOrder":
          let orderClient: Customer = {
            company: grid.api.getSelectedRows()[0].company,
            department: grid.api.getSelectedRows()[0].department,
            name: grid.api.getSelectedRows()[0].client,
            email: "",
            phone: ""
          }

          let modalFinalOrder = await this.modalController.create({
            component: OrdersInvoiceModalPage,
            componentProps: {
              "isNewPO": false,
              "supplier": grid.api.getSelectedRows()[0].supplier_code,
              "date": grid.api.getSelectedRows()[0].date,
              "client": orderClient,
              "reference": grid.api.getSelectedRows()[0].reference
            },
            cssClass: 'modal-common-css'
          });

          modalFinalOrder.onDidDismiss().then(() => {
            this.reloadAGGrid();
          });

          return await modalFinalOrder.present();
      }
    } else {
      if (grid.api.getSelectedRows().length > 1 && this.kindSelector == "fabric") {
        this.grid = grid;
        this.pdfDownloadHidden = false;
      } else {
        this.pdfDownloadHidden = true;
      }
    }
  }

  private reloadAGGrid() {
    this.loading = true;
    if (this.searchField != null) {
      this.searchService.search(this.searchField, this.kindSelector)
        .subscribe(data => {
          this.loading = false;
          this.rowData = data;
        });
    } else {
      this.searchService.searchAll(this.kindSelector).subscribe(data => {
        this.loading = false;
        this.rowData = data;
        this.gridApi.setRowData(this.rowData);
        if (this.kindSelector == "customer" || this.kindSelector == 'order' || this.kindSelector == 'finalOrder') {
          this.sizeToFit();
        }
      }, error => {
        this.alertService.showAlert('Database Error', 'Connection with database is broken, check the server status.');
      });
    }
  }

  sizeToFit() {
    this.gridApi.sizeColumnsToFit();
  }

  autoSizeAll() {
    var allColumnIds = [];
    this.gridColumnApi.getAllColumns().forEach(function (column) {
      allColumnIds.push(column.colId);
    });
    this.gridColumnApi.autoSizeColumns(allColumnIds, true);
  }

  onGridReady(grid, params) {
    this.grid = grid;
    this.gridApi = params.api; // To access the grids API
    this.gridColumnApi = params.columnApi;
    this.reloadAGGrid();
  }

  private showLoader(message) {
    this.loaderToShow = this.loadingCtrl.create({
      message: message
    }).then((res) => {
      res.present();
    });
  }

  onChange(input_kind) {
    this.rowData = [];
    this.searchField = null;
    this.kindSelector = input_kind;

    switch (this.kindSelector) {
      case "customer":
        this.placeholder = "Search customers by Department";
        this.columnDefs = [
          { headerName: "obId", field: "id", hide: true },
          { headerName: "Name", field: "name", sortable: true, filter: true },
          { headerName: "Company", field: "company", sortable: true, filter: true },
          { headerName: "Department", field: "department", sortable: true, filter: true },
          { headerName: "Email", field: "email", sortable: true, filter: true },
          { headerName: "Phone Number", field: "phone_number", sortable: true, filter: true }
        ];
        this.reloadAGGrid();
        break;
      case "supplier":
        this.placeholder = "Search suppliers by the Supplier Name or Supplier Code";
        this.columnDefs = [
          { headerName: "obId", field: "id", hide: true },
          { headerName: "Name", field: "supplier_name", sortable: true, filter: true },
          { headerName: "Code", field: "supplier_code", sortable: true, filter: true },
          { headerName: "Country", field: "country", sortable: true, filter: true },
          { headerName: "Address", field: "address", sortable: true, filter: true },
          { headerName: "Phone Number", field: "phone_number", sortable: true, filter: true },
          { headerName: "Fax", field: "fax", sortable: true, filter: true },
          { headerName: "Contact Person", field: "contact_person", sortable: true, filter: true },
          { headerName: "Postal code", field: "postal_code", sortable: true, filter: true },
          { headerName: "Fiscal Id (VAT)", field: "VAT", sortable: true, filter: true }
        ];
        this.reloadAGGrid();
        break;
      case "fabric":
        this.placeholder = "Search fabrics by the VX ID, Supplier Code, Fabric Supplier code, Comments or Fabric description";
        this.columnDefs = [
          { headerName: "obId", field: "id", hide: true },
          { headerName: "Vifrex ID", field: "vifrex_id", sortable: true, filter: true },
          { headerName: "Fabric Supp. Code", field: "fabric_supplier_code", sortable: true, filter: true },
          { headerName: "Supplier Code", field: "supplier", sortable: true, filter: true },
          { headerName: "Construction", field: "construction", sortable: true, filter: true },
          { headerName: "Type", field: "type", sortable: true, filter: true },
          { headerName: "Width", field: "width", sortable: true, filter: true },
          { headerName: "Weight", field: "weight", sortable: true, filter: true },
          { headerName: "Comments", field: "comments", sortable: true, filter: true },
          { headerName: "Price", field: "price", sortable: true, filter: true },
          { headerName: "Vifre Price", field: "vifrex_price", sortable: true, filter: true },
          { headerName: "Date", field: "date", sortable: true, filter: true },
          { headerName: "Finishing", field: "finishing", sortable: true, filter: true },
          { headerName: "Fabric Composition", field: "composition", sortable: true, filter: true }
        ];
        this.reloadAGGrid();
        this.autoSizeAll();
        break;
      case "order":
        this.placeholder = "Search orders by reference";
        this.columnDefs = [
          { headerName: "obId", field: "id", hide: true },
          { headerName: "Reference", field: "reference", sortable: true, filter: true },
          { headerName: "Client company", field: "client_company", sortable: true, filter: true },
          { headerName: "Client department", field: "client_department", sortable: true, filter: true },
          { headerName: "Client name", field: "client_name", sortable: true, filter: true },
          { headerName: "Date", field: "date", sortable: true, filter: true },
        ];
        this.reloadAGGrid();
        break;
      case "finalOrder":
        this.placeholder = "Search orders by reference, client name, department or company";
        this.columnDefs = [
          { headerName: "obId", field: "id", hide: true },
          { headerName: "Reference", field: "reference", sortable: true, filter: true },
          { headerName: "Supplier", field: "supplier_code", sortable: true, filter: true },
          { headerName: "Client company", field: "company", sortable: true, filter: true },
          { headerName: "Client department", field: "department", sortable: true, filter: true },
          { headerName: "Client name", field: "client", sortable: true, filter: true },
          { headerName: "Date", field: "date", sortable: true, filter: true },
        ];
        this.reloadAGGrid();
        break;
    }
  }
}
