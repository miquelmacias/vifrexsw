import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Customer } from 'src/app/interfaces/customer';
import { CustomersService } from 'src/app/services/customerService/customers.service';
import { AlertService } from 'src/app/services/alertService/alert.service';

@Component({
  selector: 'customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.scss'],
})
export class CustomersComponent implements OnInit {

  @Input() isThisModal: boolean = false;
  @Input() id: string;
  @Input() modalCustomer: Customer;
  tmp_customer: Customer;

  company: string;
  department: string;
  name: string;
  email: string;
  phone: string;

  constructor( 
    private customerService: CustomersService,
    private alertService: AlertService,
    private modalController: ModalController,
  ) { }

  ngOnInit(): void {
    if (this.modalCustomer !== undefined) {
      this.company = this.modalCustomer['company'];
      this.department = this.modalCustomer['department'];
      this.name = this.modalCustomer['name'];
      this.email = this.modalCustomer['email'];
      this.phone = this.modalCustomer['phone'];
    }
  }

  addCustomer(form): void {
    this.setup_values();
    this.customerService
      .getCustomer(this.tmp_customer)
      .subscribe(data => {
        if (data == null) {
          this.customerService.createCustomer(this.tmp_customer)
            .then(() => {
              form.reset();
              this.alertService.doToast("Customer added correctly");
            })
            .catch(error => {
              this.alertService.showAlert('Add customer failure', 'Customer has not been added, check all the fields and try it again.');
            })
        } else {
          this.alertService.showAlert('Customer error', 'Customer already in database');
        }
      }, error => {
        this.alertService.showAlert('Database Error', 'Connection with database is broken, check the server status.');
      });
  }

  modifyCustomer(): void {
    this.setup_values();
    this.customerService.modifyCustomer(this.tmp_customer, this.id)
      .then(() => {
        this.alertService.doToast("Item modified correctly");
        this.modalController.dismiss();
      })
      .catch(error => {
        this.alertService.showAlert("Database Error", 'Connection with database is broken, check the server status.');
        this.modalController.dismiss();
      });
  }

  removeCustomer(): void {
    this.alertService.alertToConfirm("Are you sure?", "You can't recover this customer after being erased").then(() => {
      this.customerService.removeCustomer(this.id).then(() => {
        this.alertService.doToast("Item succesfully removed");
        this.modalController.dismiss();
      })
    }).catch(error => {
      this.alertService.showAlert("Database Error", 'Connection with database is broken, check the server status.');
    })
  }

  private setup_values(): void {
    this.tmp_customer = {
      'company': this.company,
      'department': this.department,
      'name': this.name,
      'email': this.email,
      'phone': this.phone,
    }
  }

}
