import {Component} from "@angular/core";
import {ICellRendererAngularComp} from "@ag-grid-community/angular";

@Component({
    selector: 'remove-cell',
    template: `<span><button style="width: 100%; height: 100%; background: transparent; color: red; font-weight: bolder;" (click)="invokeParentMethod()" type="submit">Remove</button></span>`,
    styles: [
        `.btn {
            line-height: 0.5
        }`
    ]
})
export class RemoveRenderer implements ICellRendererAngularComp {
    public params: any;

    agInit(params: any): void {
        this.params = params;
    }

    public invokeParentMethod() {
        this.params.context.componentParent.removeMethod(this.params.node.rowIndex)
    }

    refresh(): boolean {
        return false;
    }
}
