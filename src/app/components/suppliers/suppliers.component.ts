import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';

import { Supplier } from 'src/app/interfaces/supplier';
import { SuppliersService } from 'src/app/services/supplierService/suppliers.service';
import { AlertService } from 'src/app/services/alertService/alert.service';

@Component({
  selector: 'suppliers',
  templateUrl: './suppliers.component.html',
  styleUrls: ['./suppliers.component.scss'],
})
export class SuppliersComponent implements OnInit {

  @Input() isThisModal: boolean = false;
  @Input() id: string;
  @Input() modalSupplier: Supplier;
  tmp_supplier: Supplier;

  supplier_name: string;
  supplier_code: string;
  country: string;
  address: string;
  phone_number: string;
  fax: string;
  contact_person: string;
  postal_code: string;
  VAT: string;

  constructor(
    private supplierService: SuppliersService,
    private alertService: AlertService,
    private modalController: ModalController,
  ) { }

  ngOnInit(): void {
    if (this.modalSupplier !== undefined) {
      this.supplier_name = this.modalSupplier['supplier_name'];
      this.supplier_code = this.modalSupplier['supplier_code'];
      this.country = this.modalSupplier['country'];
      this.address = this.modalSupplier['address'];
      this.phone_number = this.modalSupplier['phone_number'];
      this.fax = this.modalSupplier['fax'];
      this.contact_person = this.modalSupplier['contact_person'];
      this.postal_code = this.modalSupplier['postal_code'];
      this.VAT = this.modalSupplier['VAT'];
    }
  }

  addSupplier(form): void {
    this.setup_values();
    this.supplierService.getSupplier(this.tmp_supplier['supplier_code'])
      .subscribe(data => {
        if (data == null) {
          this.supplierService.createSupplier(this.tmp_supplier)
            .then(() => {
              form.reset();
              this.alertService.doToast("Supplier added correctly");
            })
            .catch(error => {
              this.alertService.showAlert('Database Error', 'Connection with database is broken, check the server status.');
            })
        }else {
          this.alertService.showAlert('Supplier error', 'Supplier is already in database');
        }
      }, error => {
        this.alertService.showAlert('Database Error', 'Connection with database is broken, check the server status.');
      });
  }

  modifySupplier(): void {
    this.setup_values();
    this.supplierService.modifySupplier(this.tmp_supplier, this.id)
      .then(() => {
        this.alertService.doToast("Item modified correctly");
        this.modalController.dismiss();
      })
      .catch(error => {
        this.alertService.showAlert("Database Error", 'Connection with database is broken, check the server status.');
        this.modalController.dismiss();
      });
  }

  removeSupplier(): void {
    this.alertService.alertToConfirm("Are you sure?", "You can't recover this supplier or any fabric related to it after being erased").then(() => {
      this.supplierService.removeSupplier(this.id, this.supplier_code).then(() => {
        this.alertService.doToast("Item succesfully removed");
        this.modalController.dismiss();
      }).catch(error => {
        this.alertService.showAlert("Database Error", 'Connection with database is broken, check the server status.');
      })
    })
  }

  private setup_values(): void {
    this.tmp_supplier = {
      'supplier_name': this.supplier_name,
      'supplier_code': this.supplier_code,
      'country': this.country,
      'address': this.address,
      'phone_number': this.phone_number,
      'fax': this.fax,
      'contact_person': this.contact_person,
      'postal_code': this.postal_code,
      'VAT': this.VAT,
    }
  }
}
