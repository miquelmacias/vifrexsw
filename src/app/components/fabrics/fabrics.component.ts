import { Component, OnInit, Input } from '@angular/core';
import { ModalController, LoadingController } from '@ionic/angular';

import QRCode from 'qrcode';
import * as domtoimage from 'dom-to-image'

import { Fabric } from "../../interfaces/fabric";
import { FabricsService } from 'src/app/services/fabricsService/fabrics.service';
import { AlertService } from 'src/app/services/alertService/alert.service';
import { ElectronService } from 'ngx-electron';

@Component({
  selector: 'fabrics',
  templateUrl: './fabrics.component.html',
  styleUrls: ['./fabrics.component.scss'],
})

export class FabricsComponent implements OnInit {
  @Input() isThisModal: boolean = false;
  loaderToShow: any;

  tmp_id = 'VX######';
  possible: string = "1234567890";
  lengthOfCode: number = 6;

  @Input() id: string;          // Mongo ID
  @Input() modalFabric: Fabric; // Fabric seleccionado (Modals)
  tmp_fabric: Fabric;           // Fabric guardado para "Cargar Previous"

  vx: string = this.tmp_id;
  supplier: string;
  code: string;
  composition: string;
  construction: string;
  width: string;
  weight: string;
  type: string;
  comments: string;
  date: string;
  finishing: string;
  price: string;
  fabric_description: string;
  vifrex_price: string;

  constructor(
    private fabricService: FabricsService,
    private alertService: AlertService,
    private modalController: ModalController,
    private loadingCtrl: LoadingController,
    private electronService: ElectronService
  ) { }

  ngOnInit(): void {
    if (this.modalFabric !== undefined) {
      this.vx = this.modalFabric['vx'];
      this.supplier = this.modalFabric['supplier'];
      this.code = this.modalFabric['code'];
      this.composition = this.modalFabric['composition'];
      this.construction = this.modalFabric['construction'];
      this.width = this.modalFabric['width'];
      this.weight = this.modalFabric['weight'];
      this.type = this.modalFabric['type'];
      this.comments = this.modalFabric['comments'];
      this.date = this.modalFabric['date'];
      this.finishing = this.modalFabric['finishing'];
      this.price = this.modalFabric['price'];
      this.vifrex_price = this.modalFabric['vifrex_price'];
      this.fabric_description = this.modalFabric['fabric_description'];
      this.generateQR();
    } else {
      this.date = this.get_current_date();
      this.generateQR('example');
    }
  }

  addSpace() {
    let replacementString = '% '; 
    this.composition =  
                this.composition.replace(/%|% /g, replacementString);
  }

  checkFabricCode(event) {
    if ( !this.isThisModal ) {
      this.fabricService.checkFabric('', this.code, this.supplier).subscribe(data => {
        if (data == null) {} else {
          this.alertService.showAlert('Fabrics Error', 'Fabric with same code is already in database.');
        }
      }, error => {
        this.alertService.showAlert('Database Error', 'Connection with database is broken, check the server status.');
      });
    }
  }

  addFabric(form): void {
    this.addSpace();
    this.vx = 'VX' + this.makeRandom(this.lengthOfCode, this.possible);
    this.setup_values();

    this.fabricService.checkFabric(this.vx, this.code, this.supplier).subscribe(data => {
      if (data == null) {
        this.showLoader('Saving data & Creating .PDF, please wait!');
        this.generateQR();

        let fabric: Fabric = {
          "supplier": this.supplier,
          "construction": this.construction,
          "type": this.type,
          "code": this.code,
          "width": this.width,
          "finishing": this.finishing,
          "comments": this.comments,
          "composition": this.composition,
          "weight": this.weight,
          "price": this.price,
          "vifrex_price": this.vifrex_price,
          "date": this.date,
          "vx": this.vx,
          "fabric_description": this.fabric_description
        }

        this.fabricService.createFabric(fabric).then(() => {
          const sourceElement = document.getElementById('pdf-area');
          const scale_up = 4;

          domtoimage
            .toPng(sourceElement, {
              height: sourceElement.offsetHeight * scale_up,
              width: sourceElement.offsetWidth * scale_up,
              style: {
                transform: "scale(" + scale_up + ")",
                transformOrigin: "top left",
                width: sourceElement.offsetWidth + "px",
                height: sourceElement.offsetHeight + "px"
              }
            }).then(dataUrl => {
              let imgData = new Image();
              imgData.src = dataUrl;

              this.electronService.ipcRenderer.sendSync("Tag_code", this.vx);
              this.electronService.ipcRenderer.sendSync("InitDoc", "");
              this.electronService.ipcRenderer.sendSync("Cards", imgData.src);
              this.electronService.ipcRenderer.sendSync("Tag", "");

              // Reset Form
              form.reset();
              this.vx = this.tmp_id;
              this.loadingCtrl.dismiss();
              this.alertService.doToast("Fabrics added succesfully");
            })
            .catch(error => {
              this.loadingCtrl.dismiss();
              this.alertService.showAlert('Vifrex Tag Generation Error', 'There has been a problem creating the tag. Add spaces after the percentages and try it again.');
            });

        }).catch(error => {
          this.loadingCtrl.dismiss();
          this.alertService.showAlert("Add Fabric failure", 'Fabric has not been added, supplier is not in database or some fields need to be changed.')
        })
      } else {
        this.alertService.showAlert('Fabrics Error', 'Fabric with same code is already in database.');
      }
    }, error => {
      this.alertService.showAlert('Database Error', 'Connection with database is broken, check the server status.');
    });
  }

  pasteForm(): void {
    if (this.tmp_fabric !== undefined) {
      this.supplier = this.tmp_fabric['supplier'];
      this.date = this.tmp_fabric['date'];
      this.composition = this.tmp_fabric['composition'];
      this.construction = this.tmp_fabric['construction'];
      this.width = this.tmp_fabric['width'];
      this.weight = this.tmp_fabric['weight'];
      this.type = this.tmp_fabric['type'];
      this.comments = this.tmp_fabric['comments'];
      this.finishing = this.tmp_fabric['finishing'];
      this.price = this.tmp_fabric['price'];
      this.vifrex_price = this.tmp_fabric['vifrex_price'];
      this.fabric_description = this.tmp_fabric['fabric_description'];
    } else {
      this.alertService.doToast("There are no previous fabrics");
    }
  }

  saveTag() {
    this.showLoader('Creating .PDF, please wait!');
    this.generateQR();

    const sourceElement = document.getElementById('pdf-area');
    const scale_up = 4;

    domtoimage
      .toPng(sourceElement, {
        height: sourceElement.offsetHeight * scale_up,
        width: sourceElement.offsetWidth * scale_up,
        style: {
          transform: "scale(" + scale_up + ")",
          transformOrigin: "top left",
          width: sourceElement.offsetWidth + "px",
          height: sourceElement.offsetHeight + "px"
        }
      }).then(dataUrl => {
        console.log(sourceElement.offsetHeight, sourceElement.offsetWidth);
        let imgData = new Image();
        imgData.src = dataUrl;

        this.electronService.ipcRenderer.sendSync("Tag_code", this.vx);
        this.electronService.ipcRenderer.sendSync("InitDoc", "");
        this.electronService.ipcRenderer.sendSync("Cards", imgData.src);
        this.electronService.ipcRenderer.sendSync("Tag", "");

        this.loadingCtrl.dismiss();
        this.alertService.doToast("Vifrex Tag Generated succesfully in your desktop's VXTags folder");
      })
      .catch(error => {
        this.loadingCtrl.dismiss();
        this.alertService.showAlert('Vifrex Tag Generation Error', 'There has been a problem creating the tag. Add spaces after the percentages and try it again.');
      });
  }

  modifyFabric(): void {
    this.setup_values();
    this.fabricService.modifyFabric(this.tmp_fabric, this.id)
      .then(() => {
        this.alertService.doToast("Item modified correctly");
        this.modalController.dismiss();
      })
      .catch(error => {
        this.alertService.showAlert("Database Error", 'Connection with database is broken, check the server status.');
        this.modalController.dismiss();
      });
  }

  removeFabrics(): void {
    this.alertService.alertToConfirm("Are you sure?", "You can't recover this fabric after being erased").then((ret) => {
      if (ret) {
        this.fabricService.removeFabric(this.id).then(() => {
          this.alertService.doToast("Item succesfully removed");
          this.modalController.dismiss();
        }).catch(error => {
          this.alertService.showAlert("Database Error", 'Connection with database is broken, check the server status.');
          this.modalController.dismiss();
        })
      }
    })
  }

  private setup_values(): void {
    this.date = this.get_current_date();
    this.tmp_fabric = {
      'vx': this.vx,
      'supplier': this.supplier,
      'code': this.code,
      'composition': this.composition,
      'construction': this.construction,
      'width': this.width,
      'weight': this.weight,
      'type': this.type,
      'comments': this.comments,
      'date': this.date,
      'finishing': this.finishing,
      'price': this.price,
      'vifrex_price': this.vifrex_price,
      'fabric_description': this.fabric_description,
    }
  }

  private get_current_date(): string {
    const today = new Date();
    const dd = String(today.getDate()).padStart(2, '0');
    const mm = String(today.getMonth() + 1).padStart(2, '0'); //Enero es 0
    const yyyy = today.getFullYear();

    return yyyy + '/' + mm + '/' + dd;
  }

  private makeRandom(lengthOfCode: number, possible: string): string {
    let text = "";
    for (let i = 0; i < lengthOfCode; i++) {
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    }

    return text;
  }

  private generateQR(message = this.vx): void {
    const options = {
      width: 2048,
      height: 2048,
      errorCorrectionLevel: 'H',
      type: 'svg',
      quality: 1.0,
      margin: 0,
    };

    QRCode.toDataURL(message, options, function (err, url) {
      if (err) throw err

      console.log('QR', url);
      let img = document.getElementById('qr') as HTMLImageElement;
      img.src = url;
    });
  }

  private showLoader(message) {
    this.loaderToShow = this.loadingCtrl.create({
      message: message
    }).then((res) => {
      res.present();
    });
  }
}
