const createWindowsInstaller = require('electron-winstaller').createWindowsInstaller
const path = require('path')

getInstallerConfig()
  .then(createWindowsInstaller)
  .catch((error) => {
    console.error(error.message || error)
    process.exit(1)
  })

function getInstallerConfig () {
  console.log('creating windows installer')

  return Promise.resolve({
    appDirectory: 'C:/Users/Icarus/Desktop/build/VifrexSW-win32-ia32/',
    authors: 'JM',
    noMsi: true,
    outputDirectory: 'C:/Users/Icarus/Desktop/build/windows-installer',
    exe: 'VifrexSW.exe',
    setupExe: 'VifrexSW.exe',
    setupIcon: './www/assets/icon/icon.ico',
    loadingGif: './src/assets/vifrex.gif'
  })
}