import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { HttpClientModule } from '@angular/common/http';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { IonicStorageModule } from '@ionic/storage';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthenticatorService } from './services/authenticatorService/authenticator.service';
import { FabricsService } from './services/fabricsService/fabrics.service';
import { SearchService } from './services/searchService/search.service';
import { SuppliersService } from './services/supplierService/suppliers.service';
import { CustomersService } from './services/customerService/customers.service';

import { CustomerModalPageModule } from './pages/customers-modal/customers-modal.module'
import { FabricsModalPageModule } from './pages/fabrics-modal/fabrics-modal.module';
import { SupplierModalPageModule } from './pages/suppliers-modal/suppliers-modal.module';
import { OrdersModalPageModule } from './pages/orders-modal/orders-modal.module'
import { RegisterModalPageModule } from './pages/register-modal/register-modal.module'
import { SettingsModalPageModule } from './pages/settings-modal/settings-modal.module'
import { AlertService } from './services/alertService/alert.service';
import { OrdersService } from './services/orderService/orders.service';
import { OrdersInvoicePageModule } from './pages/orders-invoice/orders-invoice.module'
import { OrdersInvoiceModalPageModule } from './pages/orders-invoice-modal/orders-invoice-modal.module'

import { NgxElectronModule } from 'ngx-electron';
import { PurchaseOrdersService } from './services/purchaseOrderService/purchase-orders.service';

@NgModule({
	declarations: [AppComponent],
	entryComponents: [],
	imports: [
		BrowserModule, 
		IonicModule.forRoot(), 
		IonicStorageModule.forRoot(),
		AppRoutingModule,
		CustomerModalPageModule,
		FabricsModalPageModule,
		SupplierModalPageModule,
		OrdersModalPageModule,
		OrdersInvoicePageModule,
		OrdersInvoiceModalPageModule,
		RegisterModalPageModule,
		SettingsModalPageModule,
		HttpClientModule,
		NgxElectronModule,
		
	],
	providers: [
		StatusBar,
		SplashScreen,
		AuthenticatorService,
		FabricsService,
		SearchService,
		SuppliersService,
		CustomersService,
		OrdersService,
		PurchaseOrdersService,
		AlertService,
		{ provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
	],
	bootstrap: [ AppComponent ],
})

export class AppModule {}
