import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { IonicModule } from '@ionic/angular';

import { FabComponent } from './components/fab/fab.component';
import { BrowserComponent } from './components/browser/browser.component';
import { FabricsComponent } from './components/fabrics/fabrics.component';
import { CustomersComponent } from './components/customers/customers.component';
import { SuppliersComponent } from './components/suppliers/suppliers.component';
import { AgGridModule } from 'ag-grid-angular';

import { RemoveRenderer } from './components/removeRenderer/remove-renderer.component'
import { RemoveRendererFabrics } from './components/removeRenderer/remove-renderer-fabrics.component';
import { RemoveRendererDeliveryPlan } from './components/removeRenderer/remove-renderer-deliveryplan.component';
import { RemoveRendererPaymentTerms } from './components/removeRenderer/remove-renderer-paymentterms.component';

@NgModule({
    imports: [
        IonicModule,
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        AgGridModule.withComponents([RemoveRenderer, RemoveRendererFabrics, RemoveRendererDeliveryPlan, RemoveRendererPaymentTerms]),
    ],
    declarations: [
        FabComponent,
        BrowserComponent,
        FabricsComponent,
        CustomersComponent,
        SuppliersComponent, 
        RemoveRenderer,
        RemoveRendererFabrics,
        RemoveRendererDeliveryPlan,
        RemoveRendererPaymentTerms,
    ],
    exports: [
        FabComponent,
        BrowserComponent,
        FabricsComponent,
        CustomersComponent,
        SuppliersComponent,
    ],
})

export class SharedModule { }