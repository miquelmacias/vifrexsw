import { TestBed } from '@angular/core/testing';

import { FabricsService } from './fabrics.service';

describe('FabricsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FabricsService = TestBed.get(FabricsService);
    expect(service).toBeTruthy();
  });
});
