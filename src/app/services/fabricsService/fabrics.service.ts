import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { HttpApiRoutes } from "../../interfaces/http-api-routes";

@Injectable({
  providedIn: 'root'
})
export class FabricsService {
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
    withCredentials: true,
  };

  constructor(
    private http: HttpClient,
  ) { }

  public createFabric(fabric) {
    return new Promise((resolve, reject) => {
      const postData = {
        "supplier": fabric['supplier'],
        "construction": fabric['construction'],
        "type": fabric['type'],
        "fabric_supplier_code": fabric['code'],
        "width": fabric['width'],
        "finishing": fabric['finishing'] == undefined ? "" : fabric['finishing'],
        "comments": fabric['comments'] == undefined ? "" : fabric['comments'],
        "composition": fabric['composition'],
        "weight": fabric['weight'],
        "price": fabric['price'] == undefined ? "" : fabric['price'],
        "vifrex_price": fabric['vifrex_price'] == undefined ? "" : fabric['vifrex_price'],
        "date": fabric['date'],
        "vifrex_id": fabric['vx'],
        "fabric_description": fabric['fabric_description'] == undefined ? "" : fabric['fabric_description']
      }

      this.http.post(HttpApiRoutes.create_fabric, postData, this.httpOptions)
        .subscribe(data => {
          if (data["error"]) {
            reject(data["error"]);
          } else {
            resolve(data["message"]);
          }
        }, error => {
          reject(error);
        });
    })
  }

  public getFabric(fabric_id) {
    let httpParams = new HttpParams().set("fabric_vifrex_id", fabric_id);
    this.httpOptions['params'] = httpParams;

    return this.http.get(HttpApiRoutes.get_fabric, this.httpOptions);
  }

  public checkFabric(fabric_id, fabric_supp_code, supp_code) {
    let httpParams = new HttpParams().set("fabric_vifrex_id", fabric_id).set("fabric_supp_code", fabric_supp_code).set("supplier_code", supp_code);
    this.httpOptions['params'] = httpParams;

    return this.http.get(HttpApiRoutes.get_check_fabric, this.httpOptions);
  }

  public modifyFabric(fabric, id) {
    return new Promise((resolve, reject) => {
      const payload = {
        "id": id,
        "edited_fields": {
          "supplier": fabric['supplier'],
          "construction": fabric['construction'],
          "type": fabric['type'],
          "fabric_supplier_code": fabric['code'],
          "width": fabric['width'],
          "finishing": fabric['finishing'] == undefined ? "" : fabric['finishing'],
          "comments": fabric['comments'] == undefined ? "" : fabric['comments'],
          "composition": fabric['composition'],
          "weight": fabric['weight'],
          "price": fabric['price'] == undefined ? "" : fabric['price'],
          "vifrex_price": fabric['vifrex_price'] == undefined ? "" : fabric['vifrex_price'],
          "date": fabric['date'],
          "vifrex_id": fabric['vx'],
          "fabric_description": fabric['fabric_description'] == undefined ? "" : fabric['fabric_description']
        }
      }

      this.http.post(HttpApiRoutes.modify_fabric, payload, this.httpOptions)
        .subscribe(data => {
          if (data["error"]) {
            reject(data["error"]);
          } else {
            resolve(data["message"]);
          }
        }, error => {
          reject(error)
        });
    });
  }

  public removeFabric(id) {
    return new Promise((resolve, reject) => {
      const payload = { "id": id }

      this.http.post(HttpApiRoutes.remove_fabric, payload, this.httpOptions)
        .subscribe(data => {
          if (data["error"]) {
            reject(data["error"]);
          } else {
            resolve(data["message"]);
          }
        }, error => {
          reject(error)
        });
    });
  }
}
