import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { HttpApiRoutes } from 'src/app/interfaces/http-api-routes';

@Injectable({
  providedIn: 'root'
})
export class OrdersService {
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
    withCredentials: true,
  };

  constructor(
    private http: HttpClient
  ) { }

  public postOrder(reference, client, data) {
    return new Promise((resolve, reject) => {
      let dataArray = [];

      let postData = {
        "reference": reference,
        "client_company": client.company,
        "client_department": client.department,
        "client_name": client.name,
        "date": this.get_current_date()
      }

      dataArray.push(postData)

      data.forEach(element => {
        let Data = {
          "reference": reference,
          "vifrex_id": element.vifrex_id,
          "sampleyardage": element.sampleyardage,
          "hanger": element.hanger,
          "comments": element.comments_order
        }
        dataArray.push(Data);
      });


      this.http.post(HttpApiRoutes.create_order, dataArray, this.httpOptions)
        .subscribe(data => {
          if (data["error"]) {
            reject(data["error"]);
          } else {
            resolve(data["message"]);
          }
        }, error => {
          reject(error);
        });
    })
  }

  public getOrderEntries(reference) {
    let httpParams = new HttpParams().set("reference", reference);
    this.httpOptions['params'] = httpParams;

    return this.http.get(HttpApiRoutes.get_orderEntries, this.httpOptions);
  }

  public getOrder(reference) {
    let httpParams = new HttpParams().set("reference", reference);
    this.httpOptions['params'] = httpParams;

    return this.http.get(HttpApiRoutes.get_order, this.httpOptions);
  }

  public removeOrder(reference) {
    return new Promise((resolve, reject) => {
      const payload = { "reference": reference }

      this.http.post(HttpApiRoutes.remove_order, payload, this.httpOptions)
        .subscribe(data => {
          if (data["error"]) {
            reject(data["error"]);
          } else {
            resolve(data["message"]);
          }
        }, error => {
          reject(error)
        });
    });
  }

  public modifyOrder(reference, data) {
    return new Promise((resolve, reject) => {
      let dataArray = [];

      data.forEach(element => {
        let Data = {
          "reference": reference,
          "vifrex_id": element.vifrex_id,
          "sampleyardage": element.sampleyardage,
          "hanger": element.hanger,
          "comments": element.comments_order
        }
        dataArray.push(Data);
      });


      this.http.post(HttpApiRoutes.modify_order, dataArray, this.httpOptions)
        .subscribe(data => {
          if (data["error"]) {
            reject(data["error"]);
          } else {
            resolve(data["message"]);
          }
        }, error => {
          reject(error);
        });
    })
  }

  private get_current_date(): string {
    const today = new Date();
    const dd = String(today.getDate()).padStart(2, '0');
    const mm = String(today.getMonth() + 1).padStart(2, '0'); //Enero es 0
    const yyyy = today.getFullYear();

    return yyyy + '/' + mm + '/' + dd;
  }
}
