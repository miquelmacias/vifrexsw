import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";

import { Storage } from "@ionic/storage";

import { HttpApiRoutes } from "../../interfaces/http-api-routes";

@Injectable({
  providedIn: "root"
})


export class AuthenticatorService {

  isLogin: boolean = false;

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
    withCredentials: true,
    // observe: 'response' as 'response',
  };

  constructor(
    private http: HttpClient,
    private storage: Storage,
  ) { }

  public login(form): Promise<unknown> {
    return new Promise((resolve, reject) => {
      const postData = {
        email: form.value.email,
        password: form.value.password,
      }

      this.http.post(HttpApiRoutes.login_user, postData, this.httpOptions)
      .subscribe(data => {
        if (data["error"]) {
          reject(data["error"]);
        } else {
          this.isLogin = true;
          this.storage.set("user", data["message"]);
          this.storage.set("email", form.value.email);
          resolve();
        }
      }, error => {
        reject(error);
      });

    setTimeout(function () { reject('We have not received any response. The server might be down 💔') }, 15000);
    });
  }

  public register(form): Promise<unknown> {
    return new Promise((resolve, reject) => {
      const postData = {
        name: form.value.name,
        email: form.value.email,
        password: form.value.password,
      }

      this.http.post(HttpApiRoutes.register_user, postData, this.httpOptions)
        .subscribe(data => {
          if (data["error"]) {
            reject(data["error"]);
          } else {
            this.storage.set("email", form.value.email);
            resolve();
          }
        }, error => {
          reject(error);
        });

      setTimeout(function () { reject('We have not received any response. The server might be down or you\'re logged in elsewhere.') }, 15000);
    })
  }

  public updatePassword(passwd): Promise<void> {
    return new Promise((resolve, reject) => {
      this.storage.get('user').then(user_id => {
        const postData = {
          id: user_id,
          password: passwd,
        }
        this.http.post(HttpApiRoutes.set_password_user, postData, this.httpOptions)
          .subscribe(data => {
            if (data["error"]) {
              reject(data["error"]);
            } else {
              resolve();
            }
          }, error => {
            reject(error);
          });

        setTimeout(function () { reject('We have not received any response.') }, 15000);
      });
    })
  }

  public logout() {
    return new Promise((resolve, reject) => {
      this.http.get(HttpApiRoutes.logout_user, this.httpOptions)
        .subscribe(data => {
          if (data["error"]) {
            console.log("First error");
            reject(data["error"]);
          } else {
            console.log("Okei");
            this.storage.remove("email");
            this.storage.remove("user");
            resolve();
          }
        }, error => {
          console.log("Second error");
          reject(error);
        });
    })
  }

  public clearStorage() {
    this.storage.clear().then(() =>
      console.log(this.storage.get("email"))
    );
  }
  public getStoredMail(): Promise<any> {
    return this.storage.get("email");
  }

  public getIsLogin(): boolean {
    return this.isLogin;
  }

  public setIsLogin(): void {
    this.isLogin = false;
  }
}

