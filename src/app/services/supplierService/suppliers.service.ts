import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { HttpApiRoutes } from "../../interfaces/http-api-routes";

@Injectable({
  providedIn: 'root'
})
export class SuppliersService {
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
    withCredentials: true,
  };

  constructor(
    private http: HttpClient,
  ) { }

  public createSupplier(supplier) {
    return new Promise((resolve, reject) => {
      const postData = {
        "supplier_name": supplier['supplier_name'],
        "supplier_code": supplier['supplier_code'],
        "country": supplier['country'],
        "address": supplier['address'],
        "phone_number": supplier['phone_number'],
        "fax": supplier['fax'] == undefined ? "" : supplier['fax'],
        "contact_person": supplier['contact_person'] == undefined ? "" : supplier['contact_person'],
        "postal_code": supplier['postal_code'] == undefined ? "" : supplier['postal_code'],
        "VAT": supplier['VAT'] == undefined ? "" : supplier['VAT']
      }

      this.http.post(HttpApiRoutes.create_supplier, postData, this.httpOptions)
        .subscribe(data => {
          if (data["error"]) {
            reject(data["error"]);
          } else {
            resolve(data["message"]);
          }
        }, error => {
          reject(error);
        });
    });
  }

  public getSupplier(supplier_code) {
    let httpParams = new HttpParams().set("supplier_code", supplier_code);
    this.httpOptions['params'] = httpParams;

    return this.http.get(HttpApiRoutes.get_supplier, this.httpOptions);
  }

  public modifySupplier(supplier, id) {
    return new Promise((resolve, reject) => {
      const payload = {
        "id": id,
        "edited_fields": {
          "supplier_name": supplier['supplier_name'],
          "supplier_code": supplier['supplier_code'],
          "country": supplier['country'],
          "address": supplier['address'],
          "phone_number": supplier['phone_number'],
          "fax": supplier['fax'] == undefined ? "" : supplier['fax'],
          "contact_person": supplier['contact_person'] == undefined ? "" : supplier['contact_person'],
          "postal_code": supplier['postal_code'] == undefined ? "" : supplier['postal_code'],
          "VAT": supplier['VAT'] == undefined ? "" : supplier['VAT']
        }
      }

      this.http.post(HttpApiRoutes.modify_supplier, payload, this.httpOptions)
        .subscribe(data => {
          if (data["error"]) {
            reject(data["error"]);
          } else {
            resolve(data["message"]);
          }
        }, error => {
          reject(error)
        });
    });
  }

  public removeSupplier(id, supplier_code) {
    return new Promise((resolve, reject) => {
      const payload = { "id": id, "supplier": supplier_code }

      this.http.post(HttpApiRoutes.remove_supplier, payload, this.httpOptions)
        .subscribe(data => {
          if (data["error"]) {
            reject(data["error"]);
          } else {
            resolve(data["message"]);
          }
        }, error => {
          reject(error)
        });
    });
  }
}
