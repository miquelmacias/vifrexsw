import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { HttpApiRoutes } from "../../interfaces/http-api-routes";

@Injectable({
  providedIn: 'root'
})
export class CustomersService {

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
    withCredentials: true,
  };

  constructor(
    private http: HttpClient
  ) { }

  public createCustomer(customer) {
    return new Promise((resolve, reject) => {
      let postData = {
        "company": customer['company'],
        "department": customer['department'],
        "name": customer['name'],
        "email": customer['email'] == undefined ? "" : customer['email'],
        "phone_number": customer['phone'] == undefined ? "" : customer['phone'],
      }

      this.http.post(HttpApiRoutes.create_customer, postData, this.httpOptions)
        .subscribe(data => {
          if (data["error"]) {
            reject(data["error"]);
          } else {
            resolve(data["message"]);
          }
        }, error => {
          reject(error);
        });
    })
  }

  public getCustomer(customer) {
    let httpParams = new HttpParams()
      .set("client_company", customer['company'])
      .set("client_department", customer['department'])
      .set("client_name", customer['name']);

    this.httpOptions['params'] = httpParams;

    return this.http.get(HttpApiRoutes.get_client, this.httpOptions);
  }

  public modifyCustomer(customer, id) {
    return new Promise((resolve, reject) => {
      const payload = {
        "id": id,
        "edited_fields": {
          "company": customer['company'],
          "department": customer['department'],
          "name": customer['name'],
          "email": customer['email'] == undefined ? "" : customer['email'],
          "phone_number": customer['phone'] == undefined ? "" : customer['phone'],
        }
      }

      this.http.post(HttpApiRoutes.modify_customer, payload, this.httpOptions)
        .subscribe(data => {
          if (data["error"]) {
            reject(data["error"]);
          } else {
            resolve(data["message"]);
          }
        }, error => {
          reject(error)
        });
    })
  }

  public removeCustomer(id) {
    return new Promise((resolve, reject) => {
      const payload = { "id": id }

      this.http.post(HttpApiRoutes.remove_customer, payload, this.httpOptions)
        .subscribe(data => {
          if (data["error"]) {
            reject(data["error"]);
          } else {
            resolve(data["message"]);
          }
        }, error => {
          reject(error)
        });
    })
  }
}
