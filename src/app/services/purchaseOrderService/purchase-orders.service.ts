import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { HttpApiRoutes } from 'src/app/interfaces/http-api-routes';

@Injectable({
  providedIn: 'root'
})
export class PurchaseOrdersService {
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
    withCredentials: true,
  };

  constructor(
    private http: HttpClient
  ) { }

  public getPurchaseOrderEntries(reference) {
    let httpParams = new HttpParams().set("reference", reference);
    this.httpOptions['params'] = httpParams;

    return this.http.get(HttpApiRoutes.get_purchaseOrderEntries, this.httpOptions);
  }

  public getLastOrder(year, month, type) {
    let httpParams = new HttpParams().set("year", year).set("month", month).set("type", type);
    this.httpOptions['params'] = httpParams;

    return this.http.get(HttpApiRoutes.get_last_order, this.httpOptions);
  }

  public postOrder(reference, supplier, date, client, summaryData, fabricsData, paymentTerms) {
    return new Promise((resolve, reject) => {
      console.log(summaryData);
      let dataArray = [];

      let postData = {
        "reference": reference,
        "supplier_code": supplier,
        "date": date,
        "client": client.name,
        "company": client.company,
        "department": client.department,
      }

      dataArray.push(postData)

      let fabricsArray = [];
      fabricsData.forEach(element => {
        let Data = {
          "reference": reference,
          "vifrex_code": element.vifrex_code,
          "color": element.color,
          "length": element.length,
          "ex_mill_date": element.ex_mill_date,
          "eta": element.eta,
          "price": element.price,
          "inco_term": element.inco_term,
        }
        fabricsArray.push(Data);
      });

      dataArray.push(fabricsArray);

      let summaryArray = [];

      let Data = {
        "reference": reference,
        "comments": summaryData,
      }
      summaryArray.push(Data);

      dataArray.push(summaryArray);

      let paymentTermsArray = [];
      paymentTerms.forEach(element => {
        let Data = {
          "reference": reference,
          "payment_term_entry": element.payment_term_entry
        }
        paymentTermsArray.push(Data);
      });

      dataArray.push(paymentTermsArray);

      this.http.post(HttpApiRoutes.create_purchase_order, dataArray, this.httpOptions)
        .subscribe(data => {
          if (data["error"]) {
            reject(data["error"]);
          } else {
            resolve(data["message"]);
          }
        }, error => {
          reject(error);
        });
    })
  }

  public modifyOrder(reference, fabricsData, summaryData, paymentTerms) {
    return new Promise((resolve, reject) => {
      let dataArray = [];

      let postData = {
        "reference": reference,
      }

      dataArray.push(postData)

      let fabricsArray = [];
      fabricsData.forEach(element => {
        let Data = {
          "reference": reference,
          "vifrex_code": element.vifrex_code,
          "color": element.color,
          "length": element.length,
          "ex_mill_date": element.ex_mill_date,
          "eta": element.eta,
          "price": element.price,
          "inco_term": element.inco_term,
        }
        fabricsArray.push(Data);
      });

      dataArray.push(fabricsArray);

      let summaryArray = [];

      let Data = {
        "reference": reference,
        "comments": summaryData,
      }
      summaryArray.push(Data);

      dataArray.push(summaryArray);

      let paymentTermsArray = [];
      paymentTerms.forEach(element => {
        let Data = {
          "reference": reference,
          "payment_term_entry": element.payment_term_entry
        }
        paymentTermsArray.push(Data);
      });

      dataArray.push(paymentTermsArray);


      this.http.post(HttpApiRoutes.modify_purchase_order, dataArray, this.httpOptions)
        .subscribe(data => {
          if (data["error"]) {
            reject(data["error"]);
          } else {
            resolve(data["message"]);
          }
        }, error => {
          reject(error);
        });
    })
  }

  public removeOrder(reference) {
    return new Promise((resolve, reject) => {
      const payload = { "reference": reference }

      this.http.post(HttpApiRoutes.remove_purchase_order, payload, this.httpOptions)
        .subscribe(data => {
          if (data["error"]) {
            reject(data["error"]);
          } else {
            resolve(data["message"]);
          }
        }, error => {
          reject(error)
        });
    });
  }

}