import { HttpClient, HttpParams, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";

import { HttpApiRoutes } from "../../interfaces/http-api-routes";

@Injectable({
	providedIn: "root"
})
export class SearchService {
	private httpOptions = {
		headers: new HttpHeaders({
			'Content-Type': 'application/json',
		}),
		withCredentials: true,
	};

	constructor(private http: HttpClient) { }

	search(searchField, kindSelector) {
		const httpParams = new HttpParams().set("search_field", searchField);
		this.httpOptions['params'] = httpParams;
		switch (kindSelector) {
			case "customer":
				return this.http.get(HttpApiRoutes.search_customer, this.httpOptions);
			case "supplier":
				return this.http.get(HttpApiRoutes.search_supplier, this.httpOptions);
			case "fabric":
				return this.http.get(HttpApiRoutes.search_fabric, this.httpOptions);
			case "order":
				return this.http.get(HttpApiRoutes.search_order, this.httpOptions);
			case "finalOrder":
				return this.http.get(HttpApiRoutes.search_finalOrder, this.httpOptions);
		}
	}

	searchAll(kindSelector) {
		switch (kindSelector) {
			case "customer":
				return this.http.get(HttpApiRoutes.get_all_customer, this.httpOptions);
			case "supplier":
				return this.http.get(HttpApiRoutes.get_all_supplier, this.httpOptions);
			case "fabric":
				return this.http.get(HttpApiRoutes.get_all_fabric, this.httpOptions);
			case "order":
				return this.http.get(HttpApiRoutes.get_all_order, this.httpOptions);
			case "finalOrder":
				return this.http.get(HttpApiRoutes.get_all_finalOrder, this.httpOptions);
		}
	}
}
