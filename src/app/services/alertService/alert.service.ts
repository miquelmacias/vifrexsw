import { Injectable } from '@angular/core';
import { AlertController, ToastController } from '@ionic/angular';
import { reject } from 'q';

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  constructor(
    private alertController: AlertController,
    private toastController: ToastController
  ) { }

  async showAlert(header, message, buttons = ['OK']) {
    const alert = await this.alertController.create({
      header: header,
      subHeader: message,
      buttons: buttons
    });

    await alert.present();
  }

  async alertToConfirm(header, message, cancelText = 'Cancel', okText = 'Accept'): Promise<any> {
    return new Promise(async (resolve) => {
      const alert = await this.alertController.create({
        header: header,
        subHeader: message,
        buttons: [
          {
            text: cancelText,
            handler: (cancel) => {
              reject();
            }
          }, {
            text: okText,
            handler: (ok) => {
              resolve(true);
            }
          }
        ]
      });
      alert.present();
    });
  }

  async doToast(message) {
    const toast = await this.toastController.create({
      message: message,
      position: "bottom",
      duration: 1000
    });

    toast.present();
  }
}
