export interface FinalOrder {
    reference: string;
    supplier_code: string;
    date: string;
    client: string;
    department: string;
    company: string;
  }