export interface Supplier {
  supplier_name: string;
  supplier_code: string;
  country: string;
  address: string;
  phone_number: string;
  fax: string;
  contact_person: string;
  postal_code: string;
  VAT: string;
}