export interface Customer {
    company: string;
    department: string;
    name: string;
    email: string;
    phone: string;
  }