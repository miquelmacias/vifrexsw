export interface Fabric {
    supplier: string;
    construction: string;
    type: string;
    code: string;
    width: string;
    finishing: string;
    comments: string;
    composition: string;
    weight: string;
    price: string;
    vifrex_price: string;
    date: string;
    vx: string;
    fabric_description: string;
  }
