export interface orderItem {
  VXid: string;
  type: string;
  comments: string;
  finishing: string;
  composition: string;
}