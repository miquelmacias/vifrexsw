export const HttpApiRoutes = {
  // AUTH REQUESTS
  login_user: 'http://167.99.180.12:8180/login',
  register_user: 'http://167.99.180.12:8180/register',
  logout_user: 'http://167.99.180.12:8180/logout',
  set_password_user: 'http://167.99.180.12:8180/set_password',

  // CREATE REQUESTS
  create_fabric: 'http://167.99.180.12:8180/fabrics/post',
  create_supplier: 'http://167.99.180.12:8180/suppliers/post',
  create_customer: 'http://167.99.180.12:8180/clients/post',
  create_order: 'http://167.99.180.12:8180/orders/post/order',
  create_purchase_order: 'http://167.99.180.12:8180/finalorders/post/finalorder',

  // MODIFY REQUESTS
  modify_fabric: 'http://167.99.180.12:8180/fabrics/update',
  modify_supplier: 'http://167.99.180.12:8180/suppliers/update',
  modify_customer: 'http://167.99.180.12:8180/clients/update',
  modify_order: 'http://167.99.180.12:8180/orders/update',
  modify_purchase_order: 'http://167.99.180.12:8180/finalorders/update',

  // REMOVE REQUESTS
  remove_fabric: 'http://167.99.180.12:8180/fabrics/remove',
  remove_supplier: 'http://167.99.180.12:8180/suppliers/remove',
  remove_customer: 'http://167.99.180.12:8180/clients/remove',
  remove_order: 'http://167.99.180.12:8180/orders/remove',
  remove_purchase_order: 'http://167.99.180.12:8180/finalorders/remove',

  // SEARCH REQUESTS
  search_fabric: 'http://167.99.180.12:8180/fabrics/search',
  search_supplier: 'http://167.99.180.12:8180/suppliers/search',
  search_customer: 'http://167.99.180.12:8180/clients/search',
  search_order: 'http://167.99.180.12:8180/orders/search',
  search_finalOrder: 'http://167.99.180.12:8180/finalorders/search',

  // GET ALL REQUESTS
  get_all_fabric: 'http://167.99.180.12:8180/fabrics/get/all',
  get_all_supplier: 'http://167.99.180.12:8180/suppliers/get/all',
  get_all_customer: 'http://167.99.180.12:8180/clients/get/all',
  get_all_order: 'http://167.99.180.12:8180/orders/get/all',
  get_all_finalOrder: 'http://167.99.180.12:8180/finalorders/get/all',

  get_last_order: 'http://167.99.180.12:8180/finalorders/get/lastcode',

  // GET REQUESTS
  get_fabric: 'http://167.99.180.12:8180/fabrics/get',
  get_check_fabric: 'http://167.99.180.12:8180/fabrics/get/check',
  get_supplier: 'http://167.99.180.12:8180/suppliers/get',
  get_client: 'http://167.99.180.12:8180/clients/get',
  get_order: 'http://167.99.180.12:8180/orders/get',
  get_orderEntries: 'http://167.99.180.12:8180/orders/get/entries',
  get_purchaseOrderEntries: 'http://167.99.180.12:8180/finalorders/get/entries',
};
