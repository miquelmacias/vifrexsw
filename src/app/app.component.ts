import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { Platform, ModalController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { SettingsModalPage } from 'src/app/pages/settings-modal/settings-modal.page'
import { AuthenticatorService } from "./services/authenticatorService/authenticator.service";

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})

export class AppComponent {
  selected: HTMLElement;
  email_element: HTMLElement;
  email: string = "";
  isOpened: boolean = false;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private router: Router,
    private authService: AuthenticatorService,
    private modalController: ModalController,
  ) {
    this.initializeApp();
    this.checkLogin();
  }

  checkLogin(): void {
    setInterval(() => {
      if(this.authService.getIsLogin) {
        this.authService.getStoredMail().then(data => {
          this.email = data;
        });
      }else {
        this.email = " ";
      }
    }, 1000);
  }

  initializeApp(): void {
    this.platform.ready().then(() => {
      this.splashScreen.hide();
      this.selected = document.getElementById('home');
      this.selected.setAttribute('class', 'sideButton selected');
      this.email_element = document.getElementById('profileEmail');
      this.authService.clearStorage();
    });
  }

  public goTo(page): void {
    this.selected.setAttribute('class', 'sideButton');
    this.selected = document.getElementById(page);
    this.selected.setAttribute('class', 'sideButton selected');
    this.router.navigate([page]);
  }

  public fabToggle(): void {
    this.isOpened = !this.isOpened;

    if (this.isOpened) {
      this.email_element.style.webkitFilter = "blur(5px)";
    } else {
      this.email_element.style.webkitFilter = "blur(0px)";
    }
  }

  public logout(): void {
    this.authService.logout().then(()=> {
      this.authService.setIsLogin();
      this.email_element.style.webkitFilter = "blur(0px)";
      this.selected.setAttribute('class', 'sideButton');
      this.selected = document.getElementById('home');
      this.selected.setAttribute('class', 'sideButton selected');
      this.router.navigate(['/login']);
    });
  }

  async settings(): Promise<void> {
    this.email_element.style.webkitFilter = "blur(0px)";
    const modalSettings = await this.modalController.create({
      component: SettingsModalPage,
      cssClass: 'modal-settings-css',
    });
    return await modalSettings.present();
  }
}

